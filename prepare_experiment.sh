#/bin/bash

solver=$(realpath "$1")
instance_base_dir="$2"

>&2 echo "solver: $solver"
>&2 echo "instances: $instance_base_dir"

for file in `find "$instance_base_dir" -name '*.txt' | sort`; do
    solution_file="$(basename -- "$file")"
    solution_file="$(realpath -- "$solution_file")"
    for ((i = 1; i<=10;i++)); do
        actual_solution_file=`printf "%s__%02d.sol" "$solution_file" $i`
        echo "tsp sh -c" "'$solver $file 30 > $actual_solution_file'"
    done;
done


