#include <top/top.h>

#include <es/es.h>

#include <ctype.h>
#include <locale.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <wchar.h>
#include <wctype.h>

#include <es/log.h>
#include <stdarg.h>

static void
print_usage(const char *name)
{
	fprintf(stderr, "usage: %s INSTANCE [TIMELIMIT]\n", name);
	fprintf(stderr, "   INSTANCE            path to instance file, columns separated by ';'\n");
	fprintf(stderr, "   TIMELIMIT           time limit in seconds\n");
	exit(EXIT_FAILURE);
}

int
main(int argc, char **argv)
{
	if (argc < 2) {
		print_usage(argv[0]);
	}

	size_t time_limit_seconds = 60;
	if (argc >= 3) {
		if (sscanf(argv[2], "%zu", &time_limit_seconds) != 1) {
			fprintf(stderr, "error: could not read time limit '%s'\n", argv[2]);
			print_usage(argv[0]);
		}
	}

	const char *filename = argv[1];

	Top_instance instance = { 0 };
	if (top_io_read_instance_file(filename, &instance) != TOP_CODE_OK) {
		fprintf(stderr, "error: could not read instance\n");
		print_usage(argv[0]);
	}

	const char *C = "main";
	es_log_name(C);

	es_log_infof(C, "solver time limit (s) = %zu", time_limit_seconds);

	es_log_infof(C, "tmax: %lf", instance.tmax);
	es_log_infof(C, "n   : %zu", instance.num_nodes);
	es_log_infof(C, "m   : %zu", instance.num_paths);

	pcg32_random_t rng = { 0 };
	pcg32_srandom_r(&rng, 1337, time(0));

	Top_solution result = { 0 };
	top_solution_init(&result, &instance);

	Top_construct_butt_cavalier_options opts = { 0 };
	opts.alpha = 0.5;
	opts.use_2opt = true;
	top_construct_butt_cavalier(&result, &opts);

	top_vnd_run(&result, &rng);

	Top_lns_options lns_opts = {
		.num_iterations = SIZE_MAX,
		.num_seconds = time_limit_seconds
	};
	top_lns_run(&result, &lns_opts, &rng);

	top_solution_print(&result, stdout);

	top_solution_deinit(&result);
	top_instance_deinit(&instance);
}
