#ifndef TOP_INTERNAL_H_INCLUDED
#define TOP_INTERNAL_H_INCLUDED 1

#include <top/top.h>

Top_profit
top_localsearch_exchange(Top_solution *solution, Top_rng *rng);

Top_profit
top_localsearch_exchange_fast(Top_solution *solution, Top_rng *rng);

#endif