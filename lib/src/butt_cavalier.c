#include <top/top.h>

#include <es/es.h>

struct Node_pair
{
	Top_node_id node1;
	Top_node_id node2;
	Top_time weighted_criterion;
	Top_time time;
};

struct Single_node
{
	Top_node_id node;
	Top_profit profit;
};

static int
compare_pair(const void *v1, const void *v2)
{
	const struct Node_pair *p1 = v1;
	const struct Node_pair *p2 = v2;
	const double cmp = p2->weighted_criterion - p1->weighted_criterion;
	if (cmp < 0.0) {
		return -1;
	}
	else if (cmp > 0.0) {
		return 1;
	}
	return 0;
}

static int
compare_single(const void *v1, const void *v2)
{
	const struct Single_node *p1 = v1;
	const struct Single_node *p2 = v2;
	const double cmp = p2->profit - p1->profit;
	if (cmp < 0.0) {
		return -1;
	}
	else if (cmp > 0.0) {
		return 1;
	}
	return 0;
}

struct State
{
	const Top_construct_butt_cavalier_options *options;
	const Top_instance *instance;
	bool *node_is_inserted;
	struct
	{
		size_t size;
		size_t capacity;
		struct Node_pair *data;
	} wgt_list;

	struct
	{
		size_t size;
		size_t capacity;
		struct Single_node *data;
	} rwd_list;
};

static void
setup_lists(struct State *state)
{
	const size_t n = state->instance->num_nodes;
	const Top_node_id s = top_instance_source_node(state->instance);
	const Top_node_id t = top_instance_target_node(state->instance);
	const Top_matrix *times = &state->instance->times;

	for (size_t i = 1; i < n; i++) {
		// Add the node to the RWD list, if it can be feasible used
		const bool is_feasible =
			top_matrix_get(times, s, i) + top_matrix_get(times, i, t) <= state->instance->tmax;
		if (!is_feasible) {
			// this not cannot be part of a node pair because it cannot be used on its own
			continue;
		}

		struct Single_node *node = &state->rwd_list.data[state->rwd_list.size++];
		node->node = i;
		node->profit = state->instance->profits[i];

		// Iterate pairs for WGT list and add them as appropriate
		for (size_t j = i + 1; j < n; j++) {
			Top_time time_pair_1 = top_matrix_get(times, s, i) + top_matrix_get(times, i, j) +
								   top_matrix_get(times, j, t);
			Top_time time_pair_2 = top_matrix_get(times, s, j) + top_matrix_get(times, i, j) +
								   top_matrix_get(times, i, t);
			size_t ii = i;
			size_t jj = j;
			Top_time time_pair = time_pair_1;
			if (time_pair_2 < time_pair_1) {
				ii = j;
				jj = i;
				time_pair = time_pair_2;
			}
			if (time_pair > state->instance->tmax) {
				// not a feasible pair
				continue;
			}

			Top_profit profits = state->instance->profits[ii] + state->instance->profits[jj];
			double weight1 = (profits / state->instance->tmax) * time_pair;
			double weight2 = (profits / time_pair) * state->instance->tmax;
			double weight =
				weight1 * state->options->alpha + weight2 * (1.0 - state->options->alpha);

			struct Node_pair *pair = &state->wgt_list.data[state->wgt_list.size++];
			pair->node1 = ii;
			pair->node2 = jj;
			pair->weighted_criterion = weight;
			pair->time = time_pair;
		}
	}

	qsort(
		state->rwd_list.data, state->rwd_list.size, sizeof(*state->rwd_list.data), compare_single);
	qsort(state->wgt_list.data, state->wgt_list.size, sizeof(*state->wgt_list.data), compare_pair);
}

Top_code
top_construct_butt_cavalier(Top_solution *solution, const Top_construct_butt_cavalier_options *opts)
{
	const Top_instance *instance = solution->instance;
	const size_t n = instance->num_nodes - 1;
	const Top_node_id s = top_instance_source_node(instance);
	const Top_node_id t = top_instance_target_node(instance);

	Top_code error = TOP_CODE_OK;

	struct State state = { opts, instance, 0, { 0 }, { 0 } };
	if (!es_vector_reserve(state.wgt_list, n * n) || !es_vector_reserve(state.rwd_list, n)) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}
	// Flag indicating for each node whether it is part of a solution
	state.node_is_inserted =
		es_memory_alloc_array(instance->num_nodes, sizeof(*state.node_is_inserted));
	if (!state.node_is_inserted) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}
	state.node_is_inserted[s] = state.node_is_inserted[t] = true;

	setup_lists(&state);

	// Construct routes

	for (size_t route_i = 0; route_i < instance->num_paths; route_i++) {
		Top_route *route = &solution->routes[route_i];
		Top_profit route_profit = 0;

		if (state.wgt_list.size > 0) {
			// Seed with the first pair
			for (size_t i = 0; i < state.wgt_list.size; i++) {
				struct Node_pair *pair = &state.wgt_list.data[i];
				if (state.node_is_inserted[pair->node1] || state.node_is_inserted[pair->node2]) {
					// at least one node already in use
					continue;
				}

				route->size = 4;
				route->sequence[3] = route->sequence[1];
				route->sequence[1] = pair->node1;
				route->sequence[2] = pair->node2;
				route->time = pair->time;

				state.node_is_inserted[pair->node1] = true;
				state.node_is_inserted[pair->node2] = true;

				Top_profit segment_profit =
					state.instance->profits[pair->node1] + state.instance->profits[pair->node2];
				solution->profit += segment_profit;
				route_profit += segment_profit;
				break;
			}

			// add more nodes..
			while (1) {
				bool performed_insertion = false;
				for (size_t wgt_index = 1; wgt_index < state.wgt_list.size; wgt_index++) {
					struct Node_pair *candidate_pair = &state.wgt_list.data[wgt_index];
					if (!(state.node_is_inserted[candidate_pair->node1] ^
							state.node_is_inserted[candidate_pair->node2])) {
						// either none of the nodes is inserted, or both
						continue;
					}

					Top_node_id candidate_node = state.node_is_inserted[candidate_pair->node1]
													 ? candidate_pair->node2
													 : candidate_pair->node1;

					Top_route_insertion_position ins =
						top_route_find_cheapest_insertion(route, &instance->times, candidate_node);

					Top_time candidate_route_time = route->time + ins.time_delta;
					if (candidate_route_time > instance->tmax) {
						// Cannot feasibly insert into the route
						continue;
					}

					top_route_insert_at(route, ins.position, candidate_node);
					route->time += ins.time_delta;
					state.node_is_inserted[candidate_node] = true;
					solution->profit += instance->profits[candidate_node];
					route_profit += instance->profits[candidate_node];
					performed_insertion = true;

					if (opts->use_2opt) {
						top_2opt_firstfit_iterate(route, &instance->times);
					}
				}
				if (!performed_insertion) {
					break;
				}
			}
		} // WGS-list processing

		// Replace by better profitable nodes
		for (size_t i = 0; i < state.rwd_list.size; i++) {
			struct Single_node *node = &state.rwd_list.data[i];
			if (state.node_is_inserted[node->node]) {
				continue;
			}
			if (node->profit > route_profit) {
				fprintf(stderr, "better node %d vs %d!\n", node->profit, route_profit);

				for (size_t i = 1; i < route->size - 1; i++) {
					state.node_is_inserted[route->sequence[i]] = false;
					solution->profit -= instance->profits[route->sequence[i]];
				}

				route->size = 3;
				route->sequence[0] = s;
				route->sequence[1] = node->node;
				route->sequence[2] = t;
				route->time = top_matrix_get(&instance->times, s, node->node) +
							  top_matrix_get(&instance->times, node->node, t);
				solution->profit += instance->profits[node->node];
				state.node_is_inserted[node->node] = true;
				break;
			}
		}
	}

error:
	es_memory_free(state.node_is_inserted);
	es_vector_free(state.wgt_list);
	es_vector_free(state.rwd_list);
	return error;
}