#include <top/top.h>

#include <es/es.h>

#include <math.h>

Top_code top_io_read_instance(FILE *fp, Top_instance *instance)
{
    char buffer[1024] = { 0 };

    struct
    {
        bool has_num_nodes;
        bool has_num_paths;
        bool has_tmax;
    } headers = { 0 };

    Top_code error = 0;
    size_t next_node_id = 0;
    while (fgets(buffer, sizeof(buffer), fp))
    {
        for (char *pos = buffer; pos;)
        {
            pos = es_cstr_find_char(pos, ';');
            if (pos)
            {
                *pos = ' ';
            }
        }
        es_cstr_trim_whitespace(buffer);
        const size_t num_seps = es_cstr_count_char(buffer, ' ');
        if (num_seps == 1)
        {
            // Probably a header token
            char *sep_pos = es_cstr_find_char(buffer, ' ');
            *sep_pos = '\0';
            es_cstr_to_lower(buffer);

            if (es_cstr_equal(buffer, "n"))
            {
                if (sscanf(sep_pos + 1, "%zu", &instance->num_nodes) != 1)
                {
                    fprintf(stderr, "could not parse header(N)\n");
                    error = TOP_CODE_IO_HEADER_ARGUMENT;
                    goto error;
                }
                headers.has_num_nodes = true;

                // we know the number of nodes, allocate space for profits
                // and the times matrix
                error = top_matrix_init(&instance->times, instance->num_nodes);
                if (error)
                    goto error;
                instance->profits = es_memory_alloc_array(instance->num_nodes, sizeof(*instance->profits));
                instance->points = es_memory_alloc_array(instance->num_nodes, sizeof(*instance->points));
                if (!instance->profits || !instance->points)
                {
                    error = TOP_CODE_ALLOCATION;
                    goto error;
                }
            }
            else if (es_cstr_equal(buffer, "m"))
            {
                if (sscanf(sep_pos + 1, "%zu", &instance->num_paths) != 1)
                {
                    fprintf(stderr, "could not parse header(M)\n");
                    error = TOP_CODE_IO_HEADER_ARGUMENT;
                    goto error;
                }
                headers.has_num_paths = true;
            }
            else if (es_cstr_equal(buffer, "tmax"))
            {
                if (sscanf(sep_pos + 1, "%lf", &instance->tmax) != 1)
                {
                    fprintf(stderr, "could not parse header(TMAX)\n");
                    error = TOP_CODE_IO_HEADER_ARGUMENT;
                    goto error;
                }
                headers.has_tmax = true;
            }
            else
            {
                fprintf(stderr, "unknown header entry '%s'\n", buffer);
                error = TOP_CODE_IO_HEADER_UNKNOWN;
                goto error;
            }
        }
        else if (num_seps == 2 && headers.has_num_nodes && headers.has_num_paths && headers.has_tmax)
        {
            // Probably a node line
            if (next_node_id >= instance->num_nodes)
            {
                error = TOP_CODE_IO_NODE_PARSE;
                goto error;
            }
            Top_point *point = &instance->points[next_node_id];
            if (sscanf(buffer, "%lf %lf %d", &point->x, &point->y, &instance->profits[next_node_id]) != 3)
            {
                fprintf(stderr, "could not parse node %zu\n", next_node_id);
                error = TOP_CODE_IO_NODE_PARSE;
                goto error;
            }
            next_node_id++;
        }
        else
        {
            fprintf(stderr, "incomplete header\n");
            error = TOP_CODE_IO_HEADER_INCOMPLETE;
            goto error;
        }
    }

    if (!(headers.has_num_nodes && headers.has_num_paths && headers.has_tmax))
    {
        error = TOP_CODE_IO_HEADER_INCOMPLETE;
        goto error;
    }

    // Calculate distances
    for (size_t i = 0; i < instance->num_nodes; i++)
    {
        top_matrix_set(&instance->times, i, i, 0.0);
        Top_point *pi = &instance->points[i];
        for (size_t j = i + 1; j < instance->num_nodes; j++)
        {
            Top_point *pj = &instance->points[j];
            const Top_time dx = pi->x - pj->x;
            const Top_time dy = pi->y - pj->y;
            const Top_time time = sqrt(dx * dx + dy * dy);
            top_matrix_set(&instance->times, i, j, time);
            top_matrix_set(&instance->times, j, i, time);
        }
    }

    return TOP_CODE_OK;

error:
    top_instance_deinit(instance);
    return error;
}

Top_code top_io_read_instance_file(const char *filename, Top_instance *instance)
{
    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        return TOP_CODE_IO_FILE_OPEN;
    }
    Top_code error = top_io_read_instance(fp, instance);
    fclose(fp);
    return error;
}