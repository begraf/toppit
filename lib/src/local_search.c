#include <top/top.h>

#include "internal.h"

#include <es/es.h>
#include <es/log.h>

Top_time
top_2opt_firstfit_perform(Top_route *route, const Top_matrix *times)
{
	for (size_t i = 1; i < route->size - 2; i++) {
		Top_node_id i1 = route->sequence[i - 1];
		Top_node_id i2 = route->sequence[i];
		Top_time i_removal_delta = -top_matrix_get(times, i1, i2);

		for (size_t j = i + 2; j < route->size; j++) {
			Top_node_id j1 = route->sequence[j - 1];
			Top_node_id j2 = route->sequence[j];
			Top_time j_removal_delta = -top_matrix_get(times, j1, j2);

			Top_time total_delta = top_matrix_get(times, i1, j1) + top_matrix_get(times, i2, j2) +
								   i_removal_delta + j_removal_delta;

			if (total_delta < 0.0) {
				// reverse segment [i,j-1]
				for (j--; i < j; i++, j--) {
					Top_node_id tmp = route->sequence[i];
					route->sequence[i] = route->sequence[j];
					route->sequence[j] = tmp;
				}

				route->time += total_delta;
				return total_delta;
			}
		}
	}
	return 0.0;
}

Top_time
top_2opt_firstfit_iterate(Top_route *route, const Top_matrix *times)
{
	Top_time total_delta = 0.0;
	while (1) {
		Top_time delta = top_2opt_firstfit_perform(route, times);
		if (delta < -0.00001) {
			total_delta += delta;
			continue;
		}

		break;
	}
	return total_delta;
}

Top_time
top_lsearch_shift_perform(Top_route *route, const Top_matrix *times, size_t max_segment_length)
{
	const size_t l = route->size - 1;
	for (size_t i = 1; i < l; i++) {
		const Top_node_id node_i = route->sequence[i];
		Top_time removal_delta_i = -top_matrix_get(times, route->sequence[i - 1], node_i);

		size_t bound_j = es_numeric_min_ulong(l, i + max_segment_length);
		for (size_t j = i; j < bound_j; j++) {
			// segment [i,j]
			const Top_node_id node_j = route->sequence[j];
			Top_time removal_delta =
				-top_matrix_get(times, node_j, route->sequence[j + 1]) + removal_delta_i +
				top_matrix_get(times, route->sequence[i - 1], route->sequence[j + 1]);

			for (size_t k = 1; k < i; k++) {
				// insert into arc [k-1, k]
				Top_time total_delta =
					-top_matrix_get(times, route->sequence[k - 1], route->sequence[k]) +
					removal_delta + top_matrix_get(times, route->sequence[k - 1], node_i) +
					top_matrix_get(times, node_j, route->sequence[k]);

				if (total_delta < -0.0001) {
					// [i,j] needs to be moved to the front of segment [k,j]
					// hence, segment [k,i-1] needs to be saved
					Top_node_id *buf = es_memory_alloc_array(route->size, sizeof(*buf));
					es_memory_copy_array_typed(buf, route->sequence + k, i - k);
					memmove(route->sequence + k, route->sequence + i,
						sizeof(*route->sequence) * (j - i + 1));
					es_memory_copy_array_typed(route->sequence + k + (j - i + 1), buf, i - k);
					route->time += total_delta;
					es_memory_free(buf);

					return total_delta;
				}
			}
			for (size_t k = j + 2; k < route->size; k++) {
				// insert into arc [k-1, k]
				Top_time total_delta =
					-top_matrix_get(times, route->sequence[k - 1], route->sequence[k]) +
					removal_delta + top_matrix_get(times, route->sequence[k - 1], node_i) +
					top_matrix_get(times, node_j, route->sequence[k]);

				if (total_delta < -0.0001) {
					es_log_tracef("shift", "found improvement of %lf", total_delta);
					// [i,j] needs to be moved to the back of [i, k-1]
					// hence, segment [j+1,k-1] needs to be saved.
					Top_node_id *buf = es_memory_alloc_array(route->size, sizeof(*buf));
					es_memory_copy_array_typed(buf, route->sequence + j + 1, k - j - 1);
					memmove(route->sequence + (k) - (j - i + 1), route->sequence + i,
						(j - i + 1) * sizeof(*route->sequence));
					es_memory_copy_array_typed(route->sequence + i, buf, k - j - 1);
					route->time += total_delta;
					es_memory_free(buf);
					return total_delta;
				}
			}
		}
	}
	return DBL_MAX;
}

Top_time
top_lsearch_shift_iterate(Top_route *route, const Top_matrix *times, size_t max_segment_length)
{
	Top_time total_delta = 0.0;
	while (1) {
		Top_time delta = top_lsearch_shift_perform(route, times, max_segment_length);
		if (delta <= -0.0001) {
			total_delta += delta;
			continue;
		}
		break;
	}
	return total_delta;
}

void
top_vnd_run(Top_solution *solution, Top_rng *rng)
{
	const char *C = "top_vnd_run";
	es_log_name(C);

	while (1) {
		const size_t m = solution->instance->num_paths;
		for (size_t i = 0; i < m; i++) {
			Top_route *route = &solution->routes[i];

			top_lsearch_shift_iterate(route, &solution->instance->times, 3);

			Top_time delta = top_2opt_firstfit_iterate(route, &solution->instance->times);
			if (delta < 0) {
				es_log_debugf(C, "2opt improved time by %lf", delta);
			}
		}

		if (top_localsearch_exchange_fast(solution, rng) > 0) {
			continue;
		}

		break;
	}

	//top_solution_is_valid(solution);
}