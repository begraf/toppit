#include <top/top.h>

#include <es/log.h>
#include <es/memory.h>
#include <es/numeric.h>

#define __USE_POSIX199309
#include <time.h>

#include <math.h>

void
top_lns_destroy_random(Top_solution *solution, size_t num_remove, Top_rng *rng)
{
	const char *C = "top_lns_destroy_random";
	es_log_name(C);

	// calculate number of planned clients
	size_t num_client = top_solution_count_active_nodes(solution);
	es_numeric_min_update_size(&num_remove, num_client);
	es_log_trace(C, "there are %zu clients of which %zu are to be removed", num_client, num_remove);

	for (size_t i = 0; i < num_remove; i++) {
		// sample removal position
		size_t remove_next = pcg32_boundedrand_r(rng, num_client);
		es_log_trace(C, "going to remove client at solution position %zu", remove_next);

		// find the route corresponding to the removal position
		Top_route *route = 0;
		size_t num_observed = 0;
		for (size_t j = 0; j < solution->instance->num_paths; j++) {
			route = &solution->routes[j];
			if (remove_next < num_observed + route->size - 2) {
				remove_next -= num_observed;
				break;
			}
			num_observed += route->size - 2;
		}
		remove_next += 1;
		es_log_trace(C, "found round, with client %d at position %zu", route->sequence[remove_next],
			remove_next);

		// remove the client
		Top_node_id removed_client = top_route_remove_at(route, remove_next);
		solution->profit -= solution->instance->profits[removed_client];
		num_client -= 1;
	}

	// update the times of all routes
	for (size_t i = 0; i < solution->instance->num_paths; i++) {
		Top_route *route = &solution->routes[i];
		route->time = top_route_calculate_time(route, &solution->instance->times);
	}
}

struct Client_bank
{
	size_t size;
	size_t capacity;
	Top_node_id *data;
};

static void
find_unplanned_clients(const Top_solution *solution, struct Client_bank *bank)
{
	bool *is_active = es_memory_alloc_array(solution->instance->num_nodes, sizeof(*is_active));
	for (size_t i = 0; i < solution->instance->num_paths; i++) {
		const Top_route *route = &solution->routes[i];
		for (size_t j = 0; j < route->size; j++) {
			is_active[route->sequence[j]] = true;
		}
	}
	for (Top_node_id i = 0; (size_t)i < solution->instance->num_nodes; i++) {
		if (!is_active[i]) {
			*es_vector_at_next(*bank) = i;
		}
	}
	es_memory_free(is_active);
}

struct Insertion_position
{
	Top_route_insertion_position route_insertion;
	Top_route *route;
	Top_node_id node;
	double criterion;
};

struct Insertion_positions
{
	size_t size;
	size_t capacity;
	struct Insertion_position *data;
};

void
collect_insertion_positions(Top_solution *solution,
	struct Client_bank *bank,
	struct Insertion_positions *positions)
{
	es_vector_clear(*positions);

	for (size_t route_i = 0; route_i < solution->instance->num_paths; route_i++) {
		Top_route *route = &solution->routes[route_i];
		for (size_t i = 0; i < es_vector_size(*bank); i++) {
			Top_node_id node = es_vector_get(*bank, i);
			Top_route_insertion_position route_insertion =
				top_route_find_cheapest_insertion(route, &solution->instance->times, node);
			if (route->time + route_insertion.time_delta > solution->instance->tmax) {
				continue;
			}

			double criterion =
				(double)solution->instance->profits[node] / (route_insertion.time_delta + 0.00001);
			struct Insertion_position *position = es_vector_at_next(*positions);
			position->route = route;
			position->criterion = criterion;
			position->route_insertion = route_insertion;
			position->node = node;
		}
	}
}

int
compare_insertion_position(const void *v1, const void *v2)
{
	const struct Insertion_position *pos1 = v1;
	const struct Insertion_position *pos2 = v2;
	if (pos1->criterion > pos2->criterion) {
		return -1;
	}
	else if (pos1->criterion < pos2->criterion) {
		return 1;
	}
	return 0;
}

void
top_lns_repair_parallel_best(Top_solution *solution, Top_rng *rng)
{
	const char *C = "top_lns_repair_parallel_best";
	es_log_name(C);

	struct Client_bank bank = { 0 };
	es_vector_reserve(bank, solution->instance->num_nodes);
	find_unplanned_clients(solution, &bank);
	es_log_trace(C, "found %zu unplanned clients", bank.size);

	struct Insertion_positions positions = { 0 };
	es_vector_reserve_nn(positions, solution->instance->num_nodes * solution->instance->num_paths);
	collect_insertion_positions(solution, &bank, &positions);
	while (1) {
		es_log_trace(C, "found %zu insertion positions", positions.size);
		if (es_vector_size(positions) == 0) {
			break;
		}

		qsort(positions.data, es_vector_size(positions), sizeof(*positions.data),
			compare_insertion_position);

		size_t selected_position_i =
			es_vector_size(positions) * pow((double)pcg32_random_r(rng) / UINT32_MAX, 3.5);
		es_numeric_min_update_size(&selected_position_i, es_vector_size(positions));
		es_log_trace(
			C, "selected position index %zu/%zu", selected_position_i, es_vector_size(positions));

		// perform insertion
		struct Insertion_position *position = es_vector_at(positions, selected_position_i);
		Top_node_id insert_node = position->node;
		Top_route *insert_route = position->route;
		top_route_insert_at(insert_route, position->route_insertion.position, insert_node);
		insert_route->time += position->route_insertion.time_delta;
		solution->profit += solution->instance->profits[insert_node];

		// remove node from client bank
		size_t index_in_client_bank = 0;
		for (size_t i = 0; i < es_vector_size(bank); i++) {
			if (es_vector_get(bank, i) == insert_node) {
				index_in_client_bank = i;
				break;
			}
		}
		es_vector_remove_at_move_back(bank, index_in_client_bank);

		// remove invalid insertion positions
		struct Insertion_position *write_end = es_vector_at(positions, 0);
		for (size_t i = 0; i < es_vector_size(positions); i++) {
			struct Insertion_position *read_end = es_vector_at(positions, i);
			if (read_end->node != insert_node && read_end->route != insert_route) {
				*write_end = *read_end;
				write_end++;
			}
		}
		positions.size = write_end - es_vector_at(positions, 0);
		// collect new insertion positions
		for (size_t i = 0; i < es_vector_size(bank); i++) {
			Top_node_id node = es_vector_get(bank, i);
			if (node == insert_node) {
				es_log_warning("LNS",
					"rediscovered node I just inserted - wtf?! pos =%zu, len =%zu", i,
					es_vector_size(bank));
				exit(1);
			}
			Top_route_insertion_position route_insertion =
				top_route_find_cheapest_insertion(insert_route, &solution->instance->times, node);
			if (insert_route->time + route_insertion.time_delta > solution->instance->tmax) {
				continue;
			}

			double criterion =
				(double)solution->instance->profits[node] / (route_insertion.time_delta + 0.00001);
			struct Insertion_position *new_position = es_vector_at_next(positions);
			new_position->route = insert_route;
			new_position->criterion = criterion;
			new_position->route_insertion = route_insertion;
			new_position->node = node;
		}
	}

	es_vector_free(positions);
	es_vector_free(bank);
}

#define TOP_LNS_CLOCK_ID CLOCK_REALTIME

void
top_lns_run(Top_solution *solution, const Top_lns_options *options, Top_rng *rng)
{
	size_t adjust_temperature_iterations = options->adjust_temperature_iterations;
	if (!adjust_temperature_iterations) {
		adjust_temperature_iterations = 100;
	}

	struct timespec start = { 0 };
	clock_gettime(TOP_LNS_CLOCK_ID, &start);
	double time_limit_secs =
		options->num_seconds + start.tv_sec + (double)start.tv_nsec / 1000000000.0;

	const Top_instance *instance = solution->instance;
	Top_solution current = { 0 };
	Top_solution candidate = { 0 };
	top_solution_init(&current, instance);
	top_solution_init(&candidate, instance);
	top_solution_copy(&current, solution);

	const size_t maximum_num_to_remove =
		es_numeric_max_size(top_solution_count_active_nodes(solution) / 2,
			solution->instance->num_nodes / solution->instance->num_paths);
	const size_t minimum_num_to_remove =
		es_numeric_min_size(1, top_solution_count_active_nodes(solution) / 10);

	double temperature = -(current.profit * 0.5) / log(0.25);
	double target_temperature = -1 / log(0.5);
	double cooling_factor = 0.99999;

	for (size_t i = 0; i < options->num_iterations; i++) {
		top_solution_copy(&candidate, &current);
		size_t num_to_remove =
			minimum_num_to_remove +
			pcg32_boundedrand_r(rng, maximum_num_to_remove - minimum_num_to_remove);
		top_lns_destroy_random(&candidate, num_to_remove, rng);
		top_lns_repair_parallel_best(&candidate, rng);

		top_vnd_run(&candidate, rng);

		if (candidate.profit >= current.profit) {
			top_solution_copy(&current, &candidate);
			if (candidate.profit > solution->profit) {
				top_solution_copy(solution, &candidate);
				es_log_info("LNS", "new best with profit %d", candidate.profit);
			}
		}
		else {
			Top_profit diff = current.profit - candidate.profit;
			if ((double)pcg32_random_r(rng) / UINT32_MAX < exp(-diff / temperature)) {
				top_solution_copy(&current, &candidate);
			}
		}

		if ((i + 1) % adjust_temperature_iterations == 0) {
			clock_gettime(TOP_LNS_CLOCK_ID, &start);
			const double current_secs = start.tv_sec + (double)start.tv_nsec / 1000000000.0;

			if (current_secs > time_limit_secs) {
				break;
			}

			const double secs_left = time_limit_secs - current_secs;
			const double secs_elapsed = options->num_seconds - secs_left;
			const double iterations_per_second = (double)i / secs_elapsed;

			es_log_info("LNS", "temperature = %lf -> %lf, current = %d time to go = %lf",
				temperature, target_temperature, current.profit, secs_left);

			const double projected_iterations = es_numeric_min_double(
				iterations_per_second * secs_left, (double)(options->num_iterations - i));

			cooling_factor = es_numeric_min_double(
				1.0, exp(log(target_temperature / temperature) / projected_iterations));
			cooling_factor = round(cooling_factor * 100000.0) / 100000.0;

			es_log_info("LNS", "iters per second = %lf, cooling = %lf", iterations_per_second,
				cooling_factor);
		}

		temperature *= cooling_factor;
	}

	top_solution_deinit(&candidate);
	top_solution_deinit(&current);
}