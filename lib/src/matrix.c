#include <top/top.h>

#include <es/es.h>

Top_code top_matrix_init(Top_matrix *mat, size_t n)
{
    es_memory_abort_null(mat, "mat required");
    *mat = (Top_matrix){ 0 };
    mat->data = es_memory_alloc_array(n * n, sizeof(*mat->data));
    mat->n = n;
    return mat->data ? TOP_CODE_OK : TOP_CODE_ALLOCATION;
}

void top_matrix_deinit(Top_matrix *mat)
{
    if (mat)
    {
        es_memory_free(mat->data);
        *mat = (Top_matrix){ 0 };
    }
}

static inline size_t cell_index(const Top_matrix *mat, size_t i_from, size_t i_to)
{
    return mat->n * i_from + i_to;
}

Top_time top_matrix_get(const Top_matrix *mat, size_t i_from, size_t i_to)
{
    return mat->data[cell_index(mat, i_from, i_to)];
}

void top_matrix_set(Top_matrix *mat, size_t i_from, size_t i_to, Top_time distance)
{
    mat->data[cell_index(mat, i_from, i_to)] = distance;
}
