#include <top/top.h>

#include <es/es.h>
#include <es/log.h>

#include <time.h>

typedef struct Top_poga_gene
{
	double x;
	double y;
} Top_poga_gene;

void
top_poga_chromosome_fill_random(Top_poga_gene *chromosome,
	const Top_instance *instance,
	Top_rng *rng)
{
	for (size_t i = 0; i < instance->num_nodes; i++) {
		Top_poga_gene *gene = &chromosome[i];
		gene->x = (double)pcg32_random_r(rng) / UINT32_MAX;
		gene->y = (double)pcg32_random_r(rng) / UINT32_MAX;
	}
}

static Top_code
top_poga_chromosome_to_solution_fast(const Top_poga_gene *chromosome, Top_solution *solution)
{
	const char *C = "top_poga_chromosome_to_solution_fast";
	es_log_name(C);

	Top_code error = TOP_CODE_OK;
	top_solution_clear(solution);

	const size_t n = solution->instance->num_nodes - 1;

	struct Successors
	{
		size_t size;
		size_t capacity;
		Top_node_id *data;
	};

	struct Successors *successors = 0;
	int *num_dominated = 0;

	struct
	{
		size_t size;
		size_t capacity;
		Top_node_id *data;
	} eligible = { 0 };
	if (!es_vector_reserve(eligible, n)) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}

	successors = es_memory_alloc_array(solution->instance->num_nodes, sizeof(*successors));
	if (!successors) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}
	for (size_t i = 1; i < n; i++) {
		if (!es_vector_reserve(successors[i], n)) {
			error = TOP_CODE_ALLOCATION;
			goto error;
		}
	}

	num_dominated = es_memory_alloc_array(solution->instance->num_nodes, sizeof(*num_dominated));
	if (!num_dominated) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}

	// Calculate graph
	for (size_t i = 1; i < n; i++) {
		const Top_poga_gene *gene_i = &chromosome[i];
		for (size_t j = i + 1; j < n; j++) {
			const Top_poga_gene *gene_j = &chromosome[j];
			if (gene_j->x > gene_i->x && gene_j->y > gene_i->y) {
				// J dominates I
				*es_vector_at_next(successors[i]) = j;
				num_dominated[j]++;
			}
			else if (gene_j->x < gene_i->x && gene_j->y < gene_i->y) {
				// I dominates J
				*es_vector_at_next(successors[j]) = i;
				num_dominated[i]++;
			}
		}
	}

	// Add initially eligible
	for (size_t i = 1; i < n; i++) {
		if (num_dominated[i] == 0) {
			*es_vector_at_next(eligible) = i;
		}
	}

	while (1) {
		if (eligible.size == 0) {
			// es_log_debugf(C, "final profit: %d", solution->profit);
			goto error;
		}

		size_t current_route_i = 0;
		while (1) {
			Top_route *route = &solution->routes[current_route_i];
			struct
			{
				Top_node_id node;
				double criterion;
				Top_route_insertion_position pos;
				size_t eligible_pos;
			} best = { .node = 0, .criterion = 0.0, .pos = { 0 }, 0 };

			for (size_t i = 0; i < eligible.size; i++) {
				Top_node_id node = es_vector_get(eligible, i);

				Top_route_insertion_position pos =
					top_route_find_cheapest_insertion(route, &solution->instance->times, node);

				if (route->time + pos.time_delta > solution->instance->tmax) {
					continue;
				}

				Top_profit profit = solution->instance->profits[node];
				double criterion = (double)profit / (pos.time_delta + 0.0001);

				if (criterion > best.criterion) {
					best.criterion = criterion;
					best.node = node;
					best.pos = pos;
					best.eligible_pos = i;
				}
			}

			if (best.node == 0) {
				if (current_route_i + 1 < solution->instance->num_paths) {
					current_route_i++;
					continue;
				}

				for (size_t i = 0; i < eligible.size; i++) {
					Top_node_id node = es_vector_get(eligible, i);
					struct Successors *sucs = &successors[node];
					for (size_t j = 0; j < es_vector_size(*sucs); j++) {
						if (num_dominated[es_vector_get(*sucs, j)] == 0) {
							es_log_errorf(C, "found node with already 0 counter, pred: %d, id %d",
								node, es_vector_get(*sucs, j));
							abort();
						}
						num_dominated[es_vector_get(*sucs, j)]--;
					}
					num_dominated[node] = -1;
				}
				eligible.size = 0;
				for (size_t i = 1; i < n; i++) {
					if (num_dominated[i] == 0) {
						*es_vector_at_next(eligible) = i;
					}
				}

				break;
			}

			{
				top_route_insert_at(route, best.pos.position, best.node);
				route->time += best.pos.time_delta;
				solution->profit += solution->instance->profits[best.node];

				// remove from eligible list
				memmove(es_vector_at(eligible, best.eligible_pos),
					es_vector_at(eligible, best.eligible_pos + 1),
					sizeof(*eligible.data) * (es_vector_size(eligible) - best.eligible_pos - 1));
				eligible.size--;

				struct Successors *sucs = &successors[best.node];
				for (size_t j = 0; j < es_vector_size(*sucs); j++) {
					Top_node_id succ_node = es_vector_get(*sucs, j);
					num_dominated[succ_node]--;
					if (num_dominated[succ_node] == 0) {
						*es_vector_at_next(eligible) = succ_node;
					}
				}
				num_dominated[best.node] = -1;

				// top_2opt_firstfit_iterate(route, &solution->instance->times);
			}
			break;
		}
	}
	// Establish best insertion position..

error:
	es_memory_free(num_dominated);
	if (successors) {
		for (size_t i = 1; i < n; i++) {
			es_vector_free(successors[i]);
		}
		es_memory_free(successors);
	}
	es_vector_free(eligible);
	return error;
}

Top_code
top_poga_chromosome_to_solution(const Top_poga_gene *chromosome, Top_solution *solution)
{
	Top_code error = TOP_CODE_OK;
	top_solution_clear(solution);

	const size_t n = solution->instance->num_nodes - 1;
	bool *node_is_active = 0;

	struct
	{
		size_t size;
		size_t capacity;
		Top_node_id *data;
	} eligible = { 0 };
	if (!es_vector_reserve(eligible, n)) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}

	node_is_active = es_memory_alloc_array(solution->instance->num_nodes, sizeof(*node_is_active));
	if (!node_is_active) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}

	while (1) {
		eligible.size = 0;
		for (size_t i = 1; i < n; i++) {
			if (node_is_active[i]) {
				continue;
			}
			const Top_poga_gene *gene_i = &chromosome[i];
			size_t num_dominated = 0;
			for (size_t j = 1; j < n; j++) {
				if (node_is_active[j]) {
					continue;
				}
				const Top_poga_gene *gene_j = &chromosome[j];
				if (gene_j->x <= gene_i->x && gene_j->y <= gene_i->y) {
					num_dominated++;
				}
			}

			if (num_dominated == 1) {
				*es_vector_at_next(eligible) = i;
			}
		}

		if (eligible.size == 0) {
			// es_log_debugf(C, "final profit: %d", solution->profit);
			goto error;
		}

		size_t current_route_i = 0;
		while (1) {
			Top_route *route = &solution->routes[current_route_i];
			struct
			{
				Top_node_id node;
				double criterion;
				Top_route_insertion_position pos;
			} best = { .node = 0, .criterion = 0.0, .pos = { 0 } };

			for (size_t i = 0; i < eligible.size; i++) {
				Top_node_id node = es_vector_get(eligible, i);

				Top_route_insertion_position pos =
					top_route_find_cheapest_insertion(route, &solution->instance->times, node);

				if (route->time + pos.time_delta > solution->instance->tmax) {
					continue;
				}

				Top_profit profit = solution->instance->profits[node];
				double criterion = (double)profit / (pos.time_delta + 0.0001);

				if (criterion > best.criterion) {
					best.criterion = criterion;
					best.node = node;
					best.pos = pos;
				}
			}

			if (best.node == 0) {
				if (current_route_i + 1 < solution->instance->num_paths) {
					current_route_i++;
					continue;
				}

				for (size_t i = 0; i < eligible.size; i++) {
					node_is_active[es_vector_get(eligible, i)] = true;
				}
				break;
			}

			{
				node_is_active[best.node] = true;
				top_route_insert_at(route, best.pos.position, best.node);
				route->time += best.pos.time_delta;
				solution->profit += solution->instance->profits[best.node];
				// top_2opt_firstfit_iterate(route, &solution->instance->times);
			}
			break;
		}
	}
	// Establish best insertion position..

error:
	es_memory_free(node_is_active);
	es_vector_free(eligible);
	return error;
}

void
top_poga_chromosome_of_solution(Top_poga_gene *chromosome, const Top_solution *solution)
{
	size_t cnt = 1;
	double increment = 1.0 / solution->instance->num_nodes;
	for (size_t i = 0; i < solution->instance->num_paths; i++) {
		Top_route *route = &solution->routes[i];
		for (size_t j = 1; j < route->size - 1; j++) {
			Top_node_id node = route->sequence[j];
			Top_poga_gene *gene = &chromosome[node];
			gene->x = cnt * increment;
			gene->y = cnt * increment;
			cnt++;
		}
	}
	for (size_t i = 0; i < solution->instance->num_nodes; i++) {
		Top_poga_gene *gene = &chromosome[i];
		if (gene->x + gene->y <= 0.0001) {
			gene->x = gene->y = cnt * increment;
		}
	}
}

typedef struct Top_poga_individual
{
	Top_profit profit;
	Top_poga_gene *chromosome;
} Top_poga_individual;

typedef struct Top_poga_population
{
	const Top_instance *instance;
	struct
	{
		size_t size;
		size_t capacity;
		Top_poga_individual *data;
	} individuals;

} Top_poga_population;

Top_poga_gene *
top_poga_chromosome_alloc(const Top_instance *instance)
{
	return es_memory_alloc_array(instance->num_nodes, sizeof(Top_poga_gene));
}

Top_code
top_poga_population_init(Top_poga_population *population, const Top_instance *instance, size_t size)
{
	*population = (Top_poga_population){ 0 };
	population->instance = instance;
	es_vector_resize(population->individuals, size);
	for (size_t i = 0; i < size; i++) {
		Top_poga_individual *individual = es_vector_at(population->individuals, i);
		individual->profit = 0;
		individual->chromosome = top_poga_chromosome_alloc(instance);
	}

	return TOP_CODE_OK;
}

void
top_poga_population_deinit(Top_poga_population *population)
{
	for (size_t i = 0; i < population->individuals.size; i++) {
		es_memory_free(es_vector_get(population->individuals, i).chromosome);
	}
	es_vector_free(population->individuals);
}

static int
compare_individual_by_profit(const void *p1, const void *p2)
{
	const Top_poga_individual *iv1 = p1;
	const Top_poga_individual *iv2 = p2;
	return iv2->profit - iv1->profit;
}

void
top_poga_population_sort(Top_poga_population *pop)
{
	qsort(pop->individuals.data, pop->individuals.size, sizeof(*pop->individuals.data),
		compare_individual_by_profit);
}

size_t
top_poga_population_rank_select(Top_poga_population *pop, Top_rng *rng)
{
	const size_t n = pop->individuals.size;
	const uint32_t upper_bound = n * (n + 1) / 2;
	const uint32_t r = pcg32_boundedrand_r(rng, upper_bound);

	uint32_t s = 0;
	for (size_t i = 0; i < n; i++) {
		s += (n - i);
		if (r <= s) {
			return i;
		}
	}
	return n - 1;
}

void
top_poga_chromosome_of_crossover(Top_poga_gene *child,
	const Top_poga_gene *g1,
	const Top_poga_gene *g2,
	const Top_instance *instance,
	Top_rng *rng)
{
	const double alpha = 0.60;
	for (size_t i = 0; i < instance->num_nodes; i++) {
		const double r = (double)pcg32_random_r(rng) / UINT32_MAX;
		child[i] = r <= alpha ? g1[i] : g2[i];
	}
}

static Top_profit
top_poga_individual_evaluate(Top_poga_individual *indv, Top_solution *solution_buffer)
{
	top_solution_clear(solution_buffer);
	top_poga_chromosome_to_solution_fast(indv->chromosome, solution_buffer);
	indv->profit = solution_buffer->profit;
	return indv->profit;
}

void
top_poga_individual_copy(Top_poga_individual *dst, const Top_poga_individual *src, size_t num_nodes)
{
	es_memory_copy_array_typed(dst->chromosome, src->chromosome, num_nodes);
	dst->profit = src->profit;
}

void
top_poga_run(const Top_instance *instance,
	const Top_poga_options *options,
	Top_rng *rng,
	Top_solution *result)
{
	const char *C = "top_poga_run";

	time_t time_start = time(0);

	Top_poga_population pops[2] = { 0 };
	top_poga_population_init(&pops[0], instance, options->population_size);
	top_poga_population_init(&pops[1], instance, options->population_size);
	size_t current_pop = 0;

	Top_solution eval_sol = { 0 };
	top_solution_init(&eval_sol, instance);

	// Initialize the population with random mutants
	{
		Top_poga_population *pop = &pops[current_pop];
		for (size_t i = 0; i < pop->individuals.size; i++) {
			Top_poga_individual *indv = es_vector_at(pop->individuals, i);
			top_poga_chromosome_fill_random(indv->chromosome, pop->instance, rng);
			top_poga_individual_evaluate(indv, &eval_sol);
			es_log_debugf("init-seed", "individual has profit %d", indv->profit);

			if (eval_sol.profit > result->profit) {
				top_solution_copy(result, &eval_sol);
			}
		}
		top_poga_population_sort(pop);
	}

	const size_t num_elite = options->elite_percentage * options->population_size;
	const size_t num_mutant = options->mutant_percentage * options->population_size;

	size_t num_generations_without_improvement = 0;

	for (size_t generation_i = 0; generation_i < options->num_generations; generation_i++) {
		num_generations_without_improvement++;

		Top_poga_population *pop_curr = &pops[current_pop];
		current_pop = (current_pop + 1) % 2;
		Top_poga_population *pop_next = &pops[current_pop];

		size_t pop_curr_insertion_i = 0;

		// Copy elite solutions
		for (size_t i = 0; i < num_elite; i++) {
			Top_poga_individual *dst = es_vector_at(pop_next->individuals, pop_curr_insertion_i);
			Top_poga_individual *src = es_vector_at(pop_curr->individuals, i);
			top_poga_individual_copy(dst, src, instance->num_nodes);
			pop_curr_insertion_i++;
		}
		// Create mutants
		for (size_t i = 0; i < num_mutant; i++) {
			Top_poga_individual *dst = es_vector_at(pop_next->individuals, pop_curr_insertion_i);
			pop_curr_insertion_i++;
			top_poga_chromosome_fill_random(dst->chromosome, instance, rng);
			top_poga_individual_evaluate(dst, &eval_sol);
		}

		// Cross over
		for (; pop_curr_insertion_i < pop_curr->individuals.size; pop_curr_insertion_i++) {
			size_t i1 = top_poga_population_rank_select(pop_curr, rng);
			size_t i2 = top_poga_population_rank_select(pop_curr, rng);

			Top_poga_individual *parent1 =
				es_vector_at(pop_curr->individuals, es_numeric_min_size(i1, i2));
			Top_poga_individual *parent2 =
				es_vector_at(pop_curr->individuals, es_numeric_max_size(i1, i2));
			Top_poga_individual *child = es_vector_at(pop_next->individuals, pop_curr_insertion_i);

			top_poga_chromosome_of_crossover(
				child->chromosome, parent1->chromosome, parent2->chromosome, instance, rng);
			top_poga_individual_evaluate(child, &eval_sol);

			if (0.001 >= (double)pcg32_random_r(rng) / UINT32_MAX) {
				top_vnd_run(&eval_sol, rng);
				child->profit = eval_sol.profit;
			}

			if (eval_sol.profit > result->profit) {
				num_generations_without_improvement = 0;
				top_solution_copy(result, &eval_sol);
			}
		}

		top_poga_population_sort(pop_next);

		es_log_infof(C, "best of next population: %d (%d)",
			es_vector_get(pop_next->individuals, 0).profit, result->profit);

		if (time(0) - time_start > (time_t)options->num_seconds) {
			es_log_infof(C, "iterations done: %zu", generation_i);
			break;
		}
	}

	top_solution_deinit(&eval_sol);
	top_poga_population_deinit(&pops[0]);
	top_poga_population_deinit(&pops[1]);
}