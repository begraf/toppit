#include "internal.h"

#include <es/log.h>
#include <es/memory.h>
#include <es/numeric.h>

#include <math.h>

#define ES_VECTOR_STRUCT(typ)                                                                      \
	struct                                                                                         \
	{                                                                                              \
		size_t size;                                                                               \
		size_t capacity;                                                                           \
		typ *data;                                                                                 \
	}
#define ES_VECTOR_STRUCT_NAMED(typ, name)                                                          \
	struct name                                                                                    \
	{                                                                                              \
		size_t size;                                                                               \
		size_t capacity;                                                                           \
		typ *data;                                                                                 \
	}

struct Exchange_position
{
	Top_route *route;
	size_t position_remove;
	size_t inactive_pos;
	Top_time time_delta;
	Top_node_id node_insert;
	Top_route_insertion_position insertion;
	Top_profit profit_delta;
};

static int
compare_exchange_position(const void *v1, const void *v2)
{
	const struct Exchange_position *pos1 = v1;
	const struct Exchange_position *pos2 = v2;
	return pos2->profit_delta - pos1->profit_delta;
}

ES_VECTOR_STRUCT_NAMED(struct Exchange_position, Exchange_positions);
ES_VECTOR_STRUCT_NAMED(Top_node_id, Inactive_nodes);

static void
collect_exchange_positions(Top_route *route,
	Top_route *route_buffer,
	const struct Inactive_nodes *inactive,
	const Top_instance *instance,
	struct Exchange_positions *positions)
{
	const Top_matrix *times = &instance->times;
	for (size_t j = 1; j < route->size - 1; j++) {
		Top_node_id i1 = route->sequence[j - 1];
		Top_node_id i2 = route->sequence[j + 1];
		Top_node_id node = route->sequence[j];

		const Top_time t_1 = -top_matrix_get(times, i1, node);
		const Top_time t_2 = top_matrix_get(times, i1, i2);
		const Top_time t_3 = -top_matrix_get(times, node, i2);
		const Top_time removal_delta = t_1 + t_2 + t_3;
		Top_profit node_profit = instance->profits[node];

		// copy route buffer
		{
			route_buffer->size = route->size - 1;
			route_buffer->time = route->time + removal_delta;
			es_memory_copy_array_typed(route_buffer->sequence, route->sequence, j);
			es_memory_copy_array_typed(
				route_buffer->sequence + j, route->sequence + j + 1, route->size - 1 - j);
		}

		for (size_t k = 0; k < inactive->size; k++) {
			Top_node_id candidate = es_vector_get(*inactive, k);
			Top_profit candidate_profit = instance->profits[candidate];
			if (candidate_profit <= node_profit) {
				continue;
			}

			Top_route_insertion_position pos =
				top_route_find_cheapest_insertion(route_buffer, times, candidate);

			Top_time total_delta = pos.time_delta + removal_delta;

			if (route->time + total_delta <= instance->tmax) {
				es_log_tracef(C,
					"improvement found, replace %d by %d for "
					"profit-delta %d",
					node, candidate, candidate_profit - node_profit);

				es_vector_extend_golden_nn(*positions);
				struct Exchange_position *position = es_vector_at_next(*positions);

				position->insertion = pos;
				position->route = route;
				position->node_insert = candidate;
				position->position_remove = j;
				position->time_delta = total_delta;
				position->profit_delta = candidate_profit - node_profit;
				position->inactive_pos = k;
			}
		}
	}
}

Top_profit
top_localsearch_exchange_fast(Top_solution *solution, Top_rng *rng)
{
	const char *C = "top_localsearch_exchange_fast";
	es_log_name(C);

	const size_t n = solution->instance->num_nodes;
	const size_t m = solution->instance->num_paths;

	bool *is_active = es_memory_alloc_array(n, sizeof(*is_active));
	for (size_t i = 0; i < m; i++) {
		Top_route *route = &solution->routes[i];
		for (size_t j = 1; j < route->size - 1; j++) {
			Top_node_id node = route->sequence[j];
			is_active[node] = true;
		}
	}

	struct Inactive_nodes inactive = { 0 };
	es_vector_reserve(inactive, n);

	for (size_t i = 1; i < n - 1; i++) {
		if (!is_active[i]) {
			*es_vector_at_next(inactive) = i;
		}
	}

	// shuffle inactive
	if (es_vector_size(inactive) > 2) {
		for (size_t i = 0; i < es_vector_size(inactive) - 1; i++) {
			const size_t j = i + pcg32_boundedrand_r(rng, inactive.size - i);
			if (j < i || j >= es_vector_size(inactive))
				abort();
			Top_node_id tmp = es_vector_get(inactive, j);
			*es_vector_at(inactive, j) = es_vector_get(inactive, i);
			*es_vector_at(inactive, i) = tmp;
		}
	}

	Top_route route_buffer = { 0 };
	top_route_init(&route_buffer, solution->instance);

	struct Exchange_positions positions = { 0 };

	Top_profit profit_delta = 0;

	for (size_t i = 0; i < m; i++) {
		Top_route *route = &solution->routes[i];
		collect_exchange_positions(route, &route_buffer, &inactive, solution->instance, &positions);
	}

	// es_log_warning(C, "found %zu exchange positions", es_vector_size(positions));

	while (1) {
		if (es_vector_size(positions) == 0) {
			break;
		}

		qsort(positions.data, es_vector_size(positions), sizeof(*positions.data),
			compare_exchange_position);

		// select position
		size_t selected_position_i =
			es_vector_size(positions) * pow((double)pcg32_random_r(rng) / UINT32_MAX, 1.5);
		es_numeric_min_update_size(&selected_position_i, es_vector_size(positions));
		struct Exchange_position position = es_vector_get(positions, selected_position_i);

		// perform change
		const Top_node_id removed_node =
			top_route_remove_at(position.route, position.position_remove);
		top_route_insert_at(position.route, position.insertion.position, position.node_insert);
		position.route->time += position.time_delta;
		solution->profit += position.profit_delta;
		*es_vector_at(inactive, position.inactive_pos) = removed_node;

		profit_delta += position.profit_delta;

		// es_log_warning(C, "performed move for profit: %d",
		// solution->instance->profits[position.node_insert] -
		// solution->instance->profits[removed_node]);

		// clear invalidated positions
		{
			struct Exchange_position *write_end = es_vector_at(positions, 0);
			for (size_t i = 0; i < es_vector_size(positions); i++) {
				struct Exchange_position *read_end = es_vector_at(positions, i);
				if (read_end->route != position.route &&
					read_end->node_insert != position.node_insert) {
					*write_end = *read_end;
					write_end++;
				}
			}
			positions.size = write_end - es_vector_at(positions, 0);
		}

		// calculate new positions
		{
			collect_exchange_positions(
				position.route, &route_buffer, &inactive, solution->instance, &positions);

			Top_node_id tmp_buf[1] = { removed_node };
			struct Inactive_nodes tmp = { .capacity = 1, .size = 1, .data = tmp_buf };

			size_t pre_size = es_vector_size(positions);
			for (size_t i = 0; i < solution->instance->num_paths; i++) {
				Top_route *route = &solution->routes[i];
				if (route == position.route) {
					continue;
				}
				collect_exchange_positions(
					route, &route_buffer, &tmp, solution->instance, &positions);
			}
			for (size_t i = pre_size; i < es_vector_size(positions); i++) {
				es_vector_at(positions, i)->inactive_pos = position.inactive_pos;
			}
		}
	}

	top_route_deinit(&route_buffer);
	es_memory_free(is_active);
	es_vector_free(inactive);
	es_vector_free(positions);

	return profit_delta;
}

Top_profit
top_localsearch_exchange(Top_solution *solution, Top_rng *rng)
{
	const char *C = "top_localsearch_exchange";
	es_log_name(C);

	const size_t n = solution->instance->num_nodes;
	const size_t m = solution->instance->num_paths;
	const Top_matrix *times = &solution->instance->times;

	bool *is_active = es_memory_alloc_array(n, sizeof(*is_active));
	for (size_t i = 0; i < m; i++) {
		Top_route *route = &solution->routes[i];
		for (size_t j = 1; j < route->size - 1; j++) {
			Top_node_id node = route->sequence[j];
			is_active[node] = true;
		}
	}

	ES_VECTOR_STRUCT(Top_node_id) inactive = { 0 };
	es_vector_reserve(inactive, n);

	for (size_t i = 1; i < n - 1; i++) {
		if (!is_active[i]) {
			*es_vector_at_next(inactive) = i;
		}
	}

	// shuffle inactive
	if (es_vector_size(inactive) > 2) {
		for (size_t i = 0; i < es_vector_size(inactive) - 1; i++) {
			const size_t j = i + pcg32_boundedrand_r(rng, inactive.size - i);
			if (j < i || j >= es_vector_size(inactive))
				abort();
			Top_node_id tmp = es_vector_get(inactive, j);
			*es_vector_at(inactive, j) = es_vector_get(inactive, i);
			*es_vector_at(inactive, i) = tmp;
		}
	}

	Top_route route_buffer = { 0 };
	top_route_init(&route_buffer, solution->instance);

	Top_profit profit_delta = 0;
start_search:
	for (size_t i = 0; i < m; i++) {
		Top_route *route = &solution->routes[i];
		for (size_t j = 1; j < route->size - 1; j++) {
			Top_node_id i1 = route->sequence[j - 1];
			Top_node_id i2 = route->sequence[j + 1];
			Top_node_id node = route->sequence[j];

			const Top_time t_1 = -top_matrix_get(times, i1, node);
			const Top_time t_2 = top_matrix_get(times, i1, i2);
			const Top_time t_3 = -top_matrix_get(times, node, i2);
			const Top_time removal_delta = t_1 + t_2 + t_3;
			Top_profit node_profit = solution->instance->profits[node];

			// copy route buffer
			{
				route_buffer.size = route->size - 1;
				route_buffer.time = route->time + removal_delta;
				es_memory_copy_array_typed(route_buffer.sequence, route->sequence, j);
				es_memory_copy_array_typed(
					route_buffer.sequence + j, route->sequence + j + 1, route->size - 1 - j);
			}

			for (size_t k = 0; k < inactive.size; k++) {
				Top_node_id candidate = es_vector_get(inactive, k);
				Top_profit candidate_profit = solution->instance->profits[candidate];
				if (candidate_profit <= node_profit) {
					continue;
				}

				Top_route_insertion_position pos =
					top_route_find_cheapest_insertion(&route_buffer, times, candidate);

				Top_time total_delta = pos.time_delta + removal_delta;

				if (route->time + total_delta <= solution->instance->tmax) {
					es_log_tracef(C,
						"improvement found, replace %d by %d for "
						"profit-delta %d",
						node, candidate, candidate_profit - node_profit);
					route->time += total_delta;

					es_memory_copy_array_typed(
						route->sequence, route_buffer.sequence, route_buffer.size);
					route->size = route_buffer.size;
					top_route_insert_at(route, pos.position, candidate);

					solution->profit += candidate_profit - node_profit;
					profit_delta += candidate_profit - node_profit;

					*es_vector_at(inactive, k) = node;
					goto start_search;
				}
			}
		}
	}

	top_route_deinit(&route_buffer);
	es_memory_free(is_active);
	es_vector_free(inactive);

	return profit_delta;
}
