#include <top/top.h>

#include <es/es.h>

#include <math.h>
#include <stdio.h>

Top_code
top_instance_init(Top_instance *instance)
{
	*instance = (Top_instance){ 0 };
	return TOP_CODE_OK;
}

void
top_instance_deinit(Top_instance *instance)
{
	es_memory_free(instance->profits);
	es_memory_free(instance->points);
	top_matrix_deinit(&instance->times);
}

Top_node_id
top_instance_source_node(const Top_instance *instance)
{
	(void)instance;
	return 0;
}

Top_node_id
top_instance_target_node(const Top_instance *instance)
{
	return instance->num_nodes - 1;
}

Top_code
top_route_init(Top_route *route, const Top_instance *instance)
{
	route->sequence = es_memory_alloc_array(instance->num_nodes, sizeof(*route->sequence));
	if (!route->sequence) {
		return TOP_CODE_ALLOCATION;
	}
	top_route_clear(route, instance);
	return TOP_CODE_OK;
}

void
top_route_clear(Top_route *route, const Top_instance *instance)
{
	Top_node_id s = top_instance_source_node(instance);
	Top_node_id t = top_instance_target_node(instance);
	route->sequence[0] = s;
	route->sequence[1] = t;
	route->size = 2;
	route->time = top_matrix_get(&instance->times, s, t);
}

void
top_route_deinit(Top_route *route)
{
	es_memory_free(route->sequence);
}

Top_route_insertion_position
top_route_find_cheapest_insertion(const Top_route *route, const Top_matrix *times, Top_node_id node)
{
	Top_route_insertion_position insertion = { .position = 0, .time_delta = TOP_TIME_MAX };

	const size_t n = route->size;
	for (size_t i = 1; i < n; i++) {
		// We try to insert on arc [i-1, i]
		const Top_node_id ni = route->sequence[i - 1];
		const Top_node_id nii = route->sequence[i];
		const Top_time t1 = top_matrix_get(times, ni, nii);
		const Top_time t2 = top_matrix_get(times, ni, node);
		const Top_time t3 = top_matrix_get(times, node, nii);
		const Top_time time_delta =  -t1 + t2 + t3;
		if (time_delta < insertion.time_delta) {
			insertion.position = i;
			insertion.time_delta = time_delta;
		}
	}

	return insertion;
}

void
top_route_insert_at(Top_route *route, size_t position, Top_node_id node)
{
	if (position < route->size) {
		memmove(&route->sequence[position + 1], &route->sequence[position],
			sizeof(*route->sequence) * (route->size - position));
	}
	route->size++;
	route->sequence[position] = node;
}

Top_node_id
top_route_remove_at(Top_route *route, size_t position)
{
	Top_node_id client = route->sequence[position];
	es_memory_carray_remove_at(route->sequence, &route->size, sizeof(*route->sequence), position);
	return client;
}

Top_time
top_route_calculate_time(const Top_route *route, const Top_matrix *times)
{
	Top_time t = 0.0;
	for (size_t i = 1; i < route->size; i++) {
		t += top_matrix_get(times, route->sequence[i - 1], route->sequence[i]);
	}
	return t;
}

Top_profit
top_route_calculate_profit(const Top_route *route, const Top_instance *instance)
{
	Top_profit p = 0;
	for (size_t i = 1; i < route->size - 1; i++) {
		p += instance->profits[route->sequence[i]];
	}
	return p;
}

Top_code
top_solution_init(Top_solution *solution, const Top_instance *instance)
{
	Top_code error = TOP_CODE_OK;

	*solution = (Top_solution){ 0 };
	solution->instance = instance;
	solution->routes = es_memory_alloc_array(instance->num_paths, sizeof(*solution->routes));
	if (!solution->routes) {
		error = TOP_CODE_ALLOCATION;
		goto error;
	}

	for (size_t i = 0; i < instance->num_paths; i++) {
		error = top_route_init(&solution->routes[i], instance);
		if (error)
			goto error;
	}

	return TOP_CODE_OK;
error:
	top_solution_deinit(solution);
	return error;
}

void
top_solution_deinit(Top_solution *solution)
{
	if (solution->routes) {
		for (size_t i = 0; i < solution->instance->num_paths; i++) {
			top_route_deinit(&solution->routes[i]);
		}
	}
	es_memory_free(solution->routes);
}

Top_code
top_solution_is_valid(Top_solution *solution)
{
	const Top_instance *instance = solution->instance;

	bool has_error = false;

	bool *node_is_active =
		es_memory_alloc_array(solution->instance->num_nodes, sizeof(*node_is_active));
	Top_profit profit = 0;
	for (size_t i = 0; i < instance->num_paths; i++) {
		Top_route *route = &solution->routes[i];
		profit += top_route_calculate_profit(route, instance);
		Top_time time = top_route_calculate_time(route, &instance->times);
		if (fabs(time - route->time) > 0.001) {
			fprintf(stderr, "[validation] inconsistent time: expected = %lf, actual = %lf\n", time,
				route->time);
			has_error = true;
		}

		for (size_t j = 1; j < route->size - 1; j++) {
			Top_node_id node = route->sequence[j];
			if (node_is_active[node]) {
				fprintf(stderr, "[validation] node %d appears multiple times in solution\n", node);
				has_error = true;
			}
			node_is_active[node] = true;
		}
	}
	es_memory_free(node_is_active);

	if (profit != solution->profit) {
		fprintf(stderr, "[validation] inconsistent profit: expected = %d, actual = %d\n", profit,
			solution->profit);
		has_error = true;
	}

	if (has_error)
		abort();

	return TOP_CODE_OK;
}

void
top_solution_print(const Top_solution *solution, FILE *fp)
{
	fprintf(fp, "profit = %d\n", solution->profit);

	size_t num_client = 0;
	for (size_t i = 0; i < solution->instance->num_paths; i++) {
		num_client += solution->routes[i].size - 2;
	}

	fprintf(fp, "client = %zu\n", num_client);

	for (size_t i = 0; i < solution->instance->num_paths; i++) {
		const Top_route *route = &solution->routes[i];
		fprintf(fp, "tour = %2zu; profit = %d; length = %0.4lf; visit = {", i,
			top_route_calculate_profit(route, solution->instance), route->time);
		if (route->size > 2) {
			fprintf(fp, "%d", route->sequence[1] - 1);
		}
		for (size_t j = 2; j < route->size - 1; j++) {
			fprintf(fp, ", %d", route->sequence[j] - 1);
		}
		fprintf(fp, "}\n");
	}
}

void
top_solution_clear(Top_solution *solution)
{
	for (size_t i = 0; i < solution->instance->num_paths; i++) {
		Top_route *route = &solution->routes[i];
		top_route_clear(route, solution->instance);
	}
	solution->profit = 0;
}

void
top_solution_copy(Top_solution *dst, const Top_solution *src)
{
	dst->profit = src->profit;
	for (size_t i = 0; i < dst->instance->num_paths; i++) {
		Top_route *dst_route = &dst->routes[i];
		const Top_route *src_route = &src->routes[i];
		dst_route->size = src_route->size;
		dst_route->time = src_route->time;
		es_memory_copy_array_typed(dst_route->sequence, src_route->sequence, src_route->size);
	}
}

size_t
top_solution_count_active_nodes(const Top_solution *solution)
{
	size_t count = 0;
	for (size_t i = 0; i < solution->instance->num_paths; i++) {
		count += solution->routes[i].size - 2;
	}
	return count;
}