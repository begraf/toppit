#ifndef TOPPIT_TOP_H_INCLUDED
#define TOPPIT_TOP_H_INCLUDED 1

#include <float.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <pcg_variants.h>

typedef pcg32_random_t Top_rng;

typedef enum Top_code
{
	TOP_CODE_OK,
	TOP_CODE_ALLOCATION,
	TOP_CODE_IO_HEADER_UNKNOWN,
	TOP_CODE_IO_HEADER_ARGUMENT,
	TOP_CODE_IO_HEADER_INCOMPLETE,
	TOP_CODE_IO_NODE_PARSE,
	TOP_CODE_IO_FILE_OPEN,
	TOP_CODE_SOLUTION_INVALID,
} Top_code;

typedef double Top_time;
typedef int32_t Top_profit;
typedef int32_t Top_node_id;

#define TOP_TIME_MAX DBL_MAX

typedef struct Top_matrix
{
	size_t n;
	Top_time *data;
} Top_matrix;

Top_code
top_matrix_init(Top_matrix *mat, size_t n);
void
top_matrix_deinit(Top_matrix *mat);
Top_time
top_matrix_get(const Top_matrix *mat, size_t i_from, size_t i_to);
void
top_matrix_set(Top_matrix *mat, size_t i_from, size_t i_to, Top_time distance);

typedef struct Top_point
{
	double x, y;
} Top_point;

typedef struct Top_instance
{
	size_t num_nodes;
	size_t num_paths;
	Top_time tmax;
	Top_profit *profits;
	Top_matrix times;
	Top_point *points;
} Top_instance;

Top_code
top_instance_init(Top_instance *instance);
void
top_instance_deinit(Top_instance *instance);
Top_node_id
top_instance_source_node(const Top_instance *instance);
Top_node_id
top_instance_target_node(const Top_instance *instance);

/// \section Routes

typedef struct Top_route
{
	size_t size;
	Top_time time;
	Top_node_id *sequence;
} Top_route;

Top_code
top_route_init(Top_route *route, const Top_instance *instance);
void
top_route_clear(Top_route *route, const Top_instance *instance);
void
top_route_deinit(Top_route *route);

typedef struct Top_route_insertion_position
{
	size_t position;
	Top_time time_delta;
} Top_route_insertion_position;

Top_route_insertion_position
top_route_find_cheapest_insertion(const Top_route *route,
	const Top_matrix *times,
	Top_node_id node);
void
top_route_insert_at(Top_route *route, size_t position, Top_node_id node);
Top_node_id
top_route_remove_at(Top_route *route, size_t position);

Top_time
top_route_calculate_time(const Top_route *route, const Top_matrix *times);
Top_profit
top_route_calculate_profit(const Top_route *route, const Top_instance *instance);

typedef struct Top_solution
{
	const Top_instance *instance;
	Top_profit profit;
	Top_route *routes;
} Top_solution;

Top_code
top_solution_init(Top_solution *solution, const Top_instance *instance);
void
top_solution_deinit(Top_solution *solution);
Top_code
top_solution_is_valid(Top_solution *solution);
void
top_solution_print(const Top_solution *solution, FILE *fp);
/// Empty all tours and reset profits
void
top_solution_clear(Top_solution *solution);
void
top_solution_copy(Top_solution *dst, const Top_solution *src);
size_t
top_solution_count_active_nodes(const Top_solution *solution);

/// \section Basic Algorithms

typedef struct
{
	double alpha;
	bool use_2opt;
} Top_construct_butt_cavalier_options;

Top_code
top_construct_butt_cavalier(Top_solution *solution,
	const Top_construct_butt_cavalier_options *opts);

Top_time
top_2opt_firstfit_perform(Top_route *route, const Top_matrix *times);
Top_time
top_2opt_firstfit_iterate(Top_route *route, const Top_matrix *times);

void
top_vnd_run(Top_solution *solution, Top_rng *rng);

/// \section Partially Ordered Genetic Algorithm

typedef struct Top_poga_options
{
	double elite_percentage;
	double mutant_percentage;
	size_t population_size;
	size_t num_generations;
	size_t num_seconds;
} Top_poga_options;

void
top_poga_run(const Top_instance *instance,
	const Top_poga_options *options,
	Top_rng *rng,
	Top_solution *result);

/// \section Large Neighborhood Search (LNS)

typedef struct Top_lns_options
{
	size_t num_iterations;
	size_t num_seconds;
	size_t adjust_temperature_iterations;
} Top_lns_options;

void
top_lns_run(Top_solution *solution, const Top_lns_options *options, Top_rng *rng);

/// \section Input / Output

Top_code
top_io_read_instance(FILE *fp, Top_instance *instance);
Top_code
top_io_read_instance_file(const char *filename, Top_instance *instance);

#endif
