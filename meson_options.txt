option(
  'log_level',
  type    : 'combo',
  choices : ['off', 'error', 'warning', 'info', 'debug', 'trace'], 
  value   : 'info'
)
