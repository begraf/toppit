#ifndef EE_AlGORITHM_H_INCLUDED
#define EE_ALGORITHM_H_INCLUDED 1

#include <stddef.h>

/// Binary search for `key` in `base` and provide a pointer to it.
/// \param key  Pointer to the key object
/// \param base  Poiter to the first object
/// \param nmemb  Number of objects in `base`
/// \param size  Size of objects
/// \param compare  Compare function, returning `< 0`, `0`, `> 0` if `a1 < a2`, `a1 == a2` or
///         `a1 > a2`.
/// \param pos {nullable}  If `key` is found, then `pos` contains a pointer to it.
///         Otherwise it will contain a pointer to the location where `key` needs to be inserted
///         into, such that the binary search conditions are met.
/// \return  If `key` is found, a pointer to its location is returned. Otherwise `NULL`.
void *es_algorithm_bsearch(void *key,
  void *base,
  size_t nmemb,
  size_t size,
  int (*compare)(const void *, const void *),
  void **pos);

/// Binary search for `key` in `base` and provide its index.
/// \param key  Pointer to the key object
/// \param base  Poiter to the first object
/// \param nmemb  Number of objects in `base`
/// \param size  Size of objects
/// \param compare  Compare function, returning `< 0`, `0`, `> 0` if `a1 < a2`, `a1 == a2` or
///         `a1 > a2`.
/// \param index {nullable}  If `key` is found, then `index` contains the position.
///         Otherwise it will contain the position that `key` needs to be inserted into, such
///         that the binary search conditions are met.
/// \return  If `key` is found, its position in `base` is returned. Otherwise `SIZE_MAX`.
size_t es_algorithm_bsearch_index(void *key,
  void *base,
  size_t nmemb,
  size_t size,
  int (*compare)(const void *a1, const void *a2),
  size_t *index);

#endif