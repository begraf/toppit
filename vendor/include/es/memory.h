#ifndef EE_NSTD_MEMORY_H_INCLUDED
#define EE_NSTD_MEMORY_H_INCLUDED 1

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// \section Control flow macros.
///
/// These macros abort or modify the control flow depending on pointers,
/// usually checking for `NULL` values.
/// Use them carefully.

/// Abort with message `msg` if `ptr == NULL`.
#define es_memory_abort_null(ptr, msg)                                                             \
	do {                                                                                       \
		if (!(ptr)) {                                                                      \
			fprintf(stderr,                                                            \
				"%s:%d (es_memory_abort_null) %s\n",                               \
				__FILE__,                                                          \
				__LINE__,                                                          \
				(msg));                                                            \
			abort();                                                                   \
		}                                                                                  \
	} while (0)

/// Return `NULL` if `ptr == NULL`.
#define es_memory_return_null(ptr)                                                                 \
	do {                                                                                       \
		if (!(ptr)) {                                                                      \
			return 0;                                                                  \
		}                                                                                  \
	} while (0)

/// \section Memory allocation - thin wrapper

/// Allocate `size` bytes of zero'd memory.
/// \param size  Number of bytes to allocate
/// \return  Pointer to allocated memory.
static inline void *
es_memory_alloc(size_t size)
{
	return calloc(1, size);
}

/// Allocate zero'd space `nmemb` items of `size`.
/// \param nmemb  Size of one item.
/// \param size  Number of items.
/// \return  Pointer to allocated memory.
static inline void *
es_memory_alloc_array(size_t nmemb, size_t size)
{
	return calloc(nmemb, size);
}

/// Allocate space `nmemb` items of `size`. The contents
/// of the memory are unspecified.
/// \param nmemb  Size of one item.
/// \param size  Number of items.
/// \return  Pointer to allocated memory.
static inline void *
es_memory_alloc_nozero(size_t size)
{
	return malloc(size);
}

/// Reallocate `ptr` for `size`.
/// \param in_ptr {nullable}  Prior pointer.
/// \param size  Size to allocate.
/// \return  Pointer to allocated memory.
static inline void *
es_memory_realloc(void *in_ptr, size_t size)
{
	return realloc(in_ptr, size);
}

/// Reallocate `ptr` for `nmemb` items of `size`.
/// \param in_ptr {nullable}  Prior pointer.
/// \param nmemb  Size of one item.
/// \param size  Number of items.
/// \return  Pointer to allocated memory.
static inline void *
es_memory_realloc_array(void *in_ptr, size_t nmemb, size_t size)
{
	return realloc(in_ptr, size * nmemb);
}

/// MACRO: Allocate one item of type `typ`.
#define es_memory_new(typ) es_memory_alloc(sizeof(typ))

/// MACRO: Allocate for `size` items of type `typ`.
#define es_memory_new_array(typ, size) es_memory_alloc_array((size), sizeof(typ))

/// \section Memory allocation API - Non Null (NN)
///
/// A thin wrapper around memory handling functions for use in
/// applications that **do not care** about *bad alloc* errors.
/// Whenever allocations fail, the program is aborted immediately.

/// Allocate `size` bytes of zero'd memory.
/// \param size  Number of bytes to allocate
/// \return  Pointer to allocated memory.
/// \note Calls `abort()` on allocation failures.
static inline void *
es_memory_alloc_nn(size_t size)
{
	void *ptr = calloc(1, size);
	if (!ptr)
		abort();
	return ptr;
}

/// Allocate zero'd space `nmemb` items of `size`.
/// \param nmemb  Size of one item.
/// \param size  Number of items.
/// \return  Pointer to allocated memory.
/// \note Calls `abort()` on allocation failures.
static inline void *
es_memory_alloc_array_nn(size_t nmemb, size_t size)
{
	void *ptr = calloc(nmemb, size);
	if (!ptr)
		abort();
	return ptr;
}

/// Allocate space `nmemb` items of `size`. The contents
/// of the memory are unspecified.
/// \param nmemb  Size of one item.
/// \param size  Number of items.
/// \return  Pointer to allocated memory.
/// \note Calls `abort()` on allocation failures.
static inline void *
es_memory_alloc_nozero_nn(size_t size)
{
	void *ptr = malloc(size);
	if (!ptr)
		abort();
	return ptr;
}

/// Reallocate `ptr` for `size`.
/// \param in_ptr {nullable}  Prior pointer.
/// \param size  Size to allocate.
/// \return  Pointer to allocated memory.
/// \note Calls `abort()` on allocation failures.
static inline void *
es_memory_realloc_nn(void *in_ptr, size_t size)
{
	void *ptr = realloc(in_ptr, size);
	if (!ptr)
		abort();
	return ptr;
}

/// Reallocate `ptr` for `nmemb` items of `size`.
/// \param in_ptr {nullable}  Prior pointer.
/// \param nmemb  Size of one item.
/// \param size  Number of items.
/// \return  Pointer to allocated memory.
/// \note Calls `abort()` on allocation failures.
static inline void *
es_memory_realloc_array_nn(void *in_ptr, size_t nmemb, size_t size)
{
	void *ptr = realloc(in_ptr, size * nmemb);
	if (!ptr)
		abort();
	return ptr;
}

/// MACRO: Allocate one item of type `typ`.
#define es_memory_new_nn(typ) es_memory_alloc_nn(sizeof(typ))

/// MACRO: Allocate for `size` items of type `typ`.
#define es_memory_new_array_nn(typ, size) es_memory_alloc_array_nn((size), sizeof(typ))

/// Free the given memory pointed to by `ptr`.
static inline void
es_memory_free(void *ptr)
{
	free(ptr);
}

/// \section Memory handling

static inline void *
es_memory_copy(void *dst, const void *src, size_t size)
{
	return memcpy(dst, src, size);
}

static inline void *
es_memory_copy_array(void *dst, const void *src, size_t nmemb, size_t size)
{
	return memcpy(dst, src, nmemb * size);
}

#define es_memory_copy_typed(dst, src) es_memory_copy((dst), (src), sizeof(*(src)))
#define es_memory_copy_array_typed(dst, src, nmemb)                                                \
	es_memory_copy_array((dst), (src), (nmemb), sizeof(*(src)))

static inline int
es_memory_compare(const void *mem1, const void *mem2, size_t size)
{
	return memcmp(mem1, mem2, size);
}

static inline int
es_memory_compare_array(const void *mem1, const void *mem2, size_t nmemb, size_t size)
{
	return memcmp(mem1, mem2, nmemb * size);
}

#define es_memory_compare_typed(mem1, mem2) es_memory_compare((mem1), (mem2), sizeof(*(mem1)))
#define es_memory_compare_array_typed(mem1, mem2, nmemb)                                           \
	es_memory_compare_array((mem1), (mem2), (nmemb), sizeof(*(mem1)))

/// \section Capacitated arrays (carray) aka vectors

static inline void
es_memory_carray_init_empty(void **data, size_t *capacity, size_t *size)
{
	*data = 0;
	*capacity = 0;
	*size = 0;
}

static inline void
es_memory_carray_free(void **data, size_t *capacity, size_t *size)
{
	if (*capacity > 0 && *data) {
		es_memory_free(*data);
		es_memory_carray_init_empty(data, capacity, size);
	}
}

static inline void
es_memory_carray_free_deep(void **data, size_t *capacity, size_t *size)
{
	if (*capacity > 0 && *data) {
		void **access = (void **)(*data);
		for (size_t i = 0; i < *size; i++) {
			es_memory_free(access[i]);
		}
		es_memory_free(*data);
		es_memory_carray_init_empty(data, capacity, size);
	}
}

#define es_vector_free(vec)                                                                        \
	es_memory_carray_free((void **)&(vec).data, &(vec).capacity, &(vec).size)
#define es_vector_free_deep(vec)                                                                   \
	es_memory_carray_free_deep((void **)&(vec).data, &(vec).capacity, &(vec).size)

static inline bool
es_memory_carray_extend_fixed(void **data,
	size_t *capacity,
	size_t size,
	size_t element_size,
	size_t n)
{
	if (*capacity <= size) {
		// at capacity's limit - reallocate
		const size_t new_capacity = *capacity + n;
		const size_t new_buffer_capacity = new_capacity * element_size;
		void *buffer = realloc(*data, new_buffer_capacity);
		if (!buffer) {
			return false;
		}
		*data = buffer;
		*capacity = new_capacity;
	}
	return true;
}

static inline bool
es_memory_carray_extend_fixed_nn(void **data,
	size_t *capacity,
	size_t size,
	size_t element_size,
	size_t n)
{
	if (!es_memory_carray_extend_fixed(data, capacity, size, element_size, n)) {
		abort();
	}
	return true;
}

#define es_vector_extend_fixed(vec, n)                                                             \
	es_memory_carray_extend_fixed(                                                             \
		(void **)&(vec).data, &(vec).capacity, (vec).size, sizeof(*(vec).data), (n))
#define es_vector_extend_fixed_nn(vec, n)                                                          \
	es_memory_carray_extend_fixed_nn(                                                          \
		(void **)&(vec).data, &(vec).capacity, (vec).size, sizeof(*(vec).data), (n))

static inline bool
es_memory_carray_extend_golden(void **data, size_t *capacity, size_t size, size_t element_size)
{
	if (*capacity <= size) {
		// at capacity's limit - reallocate
		const size_t new_capacity = 1 + *capacity + (*capacity >> 1);
		const size_t new_buffer_capacity = new_capacity * element_size;
		void *buffer = realloc(*data, new_buffer_capacity);
		if (!buffer) {
			return false;
		}
		*data = buffer;
		*capacity = new_capacity;
	}
	return true;
}

static inline bool
es_memory_carray_extend_golden_nn(void **data, size_t *capacity, size_t size, size_t element_size)
{
	if (!es_memory_carray_extend_golden(data, capacity, size, element_size)) {
		abort();
	}
	return true;
}

#define es_vector_extend_golden(vec)                                                               \
	es_memory_carray_extend_golden(                                                            \
		(void **)&(vec).data, &(vec).capacity, (vec).size, sizeof(*(vec).data))
#define es_vector_extend_golden_nn(vec)                                                            \
	es_memory_carray_extend_golden_nn(                                                         \
		(void **)&(vec).data, &(vec).capacity, (vec).size, sizeof(*(vec).data))

static inline bool
es_memory_carray_reserve(void **data, size_t *capacity, size_t element_size, size_t min_capacity)
{
	if (*capacity < min_capacity) {
		const size_t new_buffer_capacity = min_capacity * element_size;
		void *buffer = realloc(*data, new_buffer_capacity);
		if (!buffer) {
			return false;
		}
		*data = buffer;
		*capacity = min_capacity;
	}
	return true;
}

static inline bool
es_memory_carray_reserve_nn(void **data, size_t *capacity, size_t element_size, size_t min_capacity)
{
	if (!es_memory_carray_reserve(data, capacity, element_size, min_capacity)) {
		abort();
	}
	return true;
}

#define es_vector_reserve(vec, n)                                                                  \
	es_memory_carray_reserve((void **)&(vec).data, &(vec).capacity, sizeof(*(vec).data), (n))
#define es_vector_reserve_nn(vec, n)                                                               \
	es_memory_carray_reserve_nn((void **)&(vec).data, &(vec).capacity, sizeof(*(vec).data), (n))

static inline bool
es_memory_carray_resize(void **data,
	size_t *capacity,
	size_t *size,
	size_t element_size,
	size_t new_size)
{
	if (!es_memory_carray_reserve(data, capacity, element_size, new_size)) {
		return false;
	}
	*size = new_size;
	return true;
}

static inline bool
es_memory_carray_resize_nn(void **data,
	size_t *capacity,
	size_t *size,
	size_t element_size,
	size_t new_size)
{
	if (!es_memory_carray_resize(data, capacity, size, element_size, new_size)) {
		abort();
	}
	return true;
}

#define es_vector_resize(vec, n)                                                                   \
	es_memory_carray_resize(                                                                   \
		(void **)&(vec).data, &(vec).capacity, &(vec).size, sizeof(*(vec).data), (n))
#define es_vector_resize_nn(vec, n)                                                                \
	es_memory_carray_resize_nn(                                                                \
		(void **)&(vec).data, &(vec).capacity, &(vec).size, sizeof(*(vec).data), (n))

static inline void
es_memory_carray_remove_at(void *data, size_t *size, size_t element_size, size_t position)
{
	if (position >= *size) {
		return;
	}

	char *target = (char *)data + position * element_size;
	memmove(target, target + element_size, element_size * (*size - position - 1));
	*size -= 1;
}

static inline void
es_memory_carray_remove_at_move_back(void *data, size_t *size, size_t element_size, size_t position)
{
	if (position >= *size) {
		return;
	}

	if (position + 1 < *size) {
		// there is an element behind the element to be removed,
		// therefore we move it into the removed slot
		char *target = (char *)data + position * element_size;
		char *back = (char *)data + (*size - 1) * element_size;
		memcpy(target, back, element_size);
	}

	*size -= 1;
}

#define es_vector_remove_at(vec, pos)                                                              \
	es_memory_carray_remove_at((void *)(vec).data, &(vec).size, sizeof(*(vec).data), (pos))
#define es_vector_remove_at_move_back(vec, pos)                                                    \
	es_memory_carray_remove_at_move_back(                                                      \
		(void *)(vec).data, &(vec).size, sizeof(*(vec).data), (pos))

#define es_vector_clear(vec)                                                                       \
	do {                                                                                       \
		(vec).size = 0;                                                                    \
	} while (0)
#define es_vector_size(vec) ((vec).size)
#define es_vector_capacity(vec) ((vec).capacity)
#define es_vector_get(vec, i) ((vec).data[i])
#define es_vector_at(vec, i) (&(vec).data[i])
#define es_vector_at_next(vec) (&(vec).data[(vec).size++])

/// \section Reference counting

struct Memory_rc_object
{
	size_t count;
	char object[];
};

static inline void *
es_memory_rc_alloc_nn(size_t n)
{
	struct Memory_rc_object *mem = es_memory_alloc_nn(sizeof(*mem) + n);
	mem->count = 1;
	return &mem->object;
}

static inline size_t
es_memory_rc_count(void *mem)
{
	struct Memory_rc_object *obj =
		(void *)((char *)mem - offsetof(struct Memory_rc_object, object));
	return obj->count;
}

static inline void
es_memory_rc_incref(void *mem)
{
	struct Memory_rc_object *obj =
		(void *)((char *)mem - offsetof(struct Memory_rc_object, object));
	obj->count++;
}

static inline void
es_memory_rc_decref(void *mem)
{
	struct Memory_rc_object *obj =
		(void *)((char *)mem - offsetof(struct Memory_rc_object, object));
	obj->count--;
	if (obj->count == 0) {
		es_memory_free(obj);
	}
}

#endif
