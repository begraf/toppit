#ifndef EE_STRING_H_INCLUDED
#define EE_STRING_H_INCLUDED

#include <stdbool.h>
#include <stdlib.h>

/// \section Properties

/// Tests whether a string is empty (has size 0).
/// \param string {nullable}  String to check.
/// \return `true` if `string == NULL || string[0] == '\0'`.
bool es_cstr_is_empty(const char *string);

/// Calculates the length of `string` in bytes, without NUL-byte.
size_t es_cstr_size(const char *string);

/// Tests whether `string` is all ASCII.
/// \param string  String to check.
/// \return `true` iff `string` is composed of ASCII characters only.
bool es_cstr_is_ascii(const char *string);

/// \section Mutation

/// Appends writes `src` to `dst` and returns a pointer to the
/// `NUL`-byte of `dst`.
char *es_cstr_append(char *dst, const char *src);

/// Trim whitespace on both ends.
/// \param string  String to trim
/// \return  Input pointer `string`.
/// \example
/// ~~~
/// char *c = es_cstr_trim_whitespace(string_cstr_dup("  hello world  "));
/// assert(es_cstr_equal(c, "hello world"));
/// ~~~
char *es_cstr_trim_whitespace(char *string);

/// Trim whitespace on the left side.
/// \param string  String to trim
/// \return  Input pointer `string`.
char *es_cstr_trim_whitespace_left(char *string);

/// Trim whitespace on the right side.
/// \param string  String to trim
/// \return  Input pointer `string`.
char *es_cstr_trim_whitespace_right(char *string);

/// Check if two strings are equal.
/// \return
///     `true` if `strcmp(string1, string2) == 0`, `false` otherwise.
bool es_cstr_equal(const char *string1, const char *string2);

/// Compare two strings.
/// \return
///     A number lower than zero if `string1 < string2`, a number larger than zero
///     if `string1 > string2` and zero if both are equal.
int es_cstr_compare(const char *string1, const char *string2);

/// Skips over leading whitespace and returns the first non-whitespace
/// position.
/// \return
///     Pointer to first non-whitespace position, which may be the NUL-byte.
/// \example
///     `es_cstr_skip_whitespace("  hallo")` yields `"hallo"`.
///     `es_cstr_skip_whitespace("  ")` yields `""`.
char *es_cstr_skip_whitespace(char *string);

/// === Allocating functions. ===

/// Duplicate the given string.
/// \return
///     A copy of `string` or `NULL` in case of allocation errors.
char *es_cstr_dup(const char *string);

/// \section Joining and splitting

/// Joins `strings` into a single string.
/// \return
///     A single string joining all given strings or NULL if
///     the allocation failed.
/// \example
///     `es_cstr_join((const char **){"hallo", "welt"})` yields
///     `"hallowelt"`.
char *es_cstr_join(const char **strings, size_t n);

/// Joins `strings` into a single string, separated by `separator`.
/// \return
///     A single string joining all given strings separated by `separator`
///     or `NULL` if the allocation failed.
/// \example
///     `es_cstr_join_by((const char *[]){"hallo", "welt"}, "--")` yields
///     `"hallo--welt"`.
char *es_cstr_join_by(const char **strings, size_t n, const char *separator);

/// \section Searching and counting

/// Find the first occurrence of `key` in `str`.
/// \param str {nullable}  String to search in.
/// \param key  Character to find.
/// \return  A pointer to the first occurrence of `key` if `key` is
///     part of `str`. Otherwise or if `str == NULL`, `NULL` is returned.
char *es_cstr_find_char(const char *str, char key);

/// Find the first position in `str` satisfying `pred`.
/// \param str {nullable}  String to search in.
/// \param pred  Character predicate function.
/// \return  A pointer to the first occurrence of a character satisfying
///     `pred` or `NULL` if there is none or `str == NULL`.
char *es_cstr_find_char_by(const char *str, int (*pred)(int));

/// Count the occurrences of `key` in `str`.
/// \param str {nullable}  String to count in.
/// \param key  Character to count.
/// \return  The number of occurrences of `key` in `str`.
///     If `str == NULL`, then `0` is returned.
size_t es_cstr_count_char(const char *str, char key);

/// Count the characters in `str` that satisfy `pred`.
/// \param str {nullable}  String to count in.
/// \param pred  Character predicate function.
/// \return  The number of characters satisfying `pred` in `str`.
///     If `str == NULL`, then `0` is returned.
size_t es_cstr_count_char_by(const char *str, int (*pred)(int));

/// \section Conversions and filters

/// Convert the characters of `str` to lower case.
/// \param str {nullable}  String.
/// \note  If `str == NULL`, no action is performed.
void es_cstr_to_lower(char *str);

/// Convert the characters of `str` to upper case.
/// \param str {nullable}  String.
/// \note  If `str == NULL`, no action is performed.
void es_cstr_to_upper(char *str);

/// \section Non-null API

/// Duplicate the given string.
/// \return  A copy of `string`.
/// \note  Calls `abort()` in case of allocation errors.
char *es_cstr_dup_nn(const char *string);

/// \see `es_cstr_join`
/// \note Calls `abort()` in case of allocation errors.
char *es_cstr_join_nn(const char **strings, size_t n);

/// \see `es_cstr_join_by`
/// \note Calls `abort()` in case of allocation errors.
char *es_cstr_join_by_nn(const char **strings, size_t n, const char *separator);

#endif
