#ifndef EE_ESTD_NUMERIC_H_INCLUDED
#define EE_ESTD_NUMERIC_H_INCLUDED 1

#include <stdint.h>
#include <stddef.h>

/// \section Minimum and Maximum

#define EE_MAKE_FUNCTION_NUMERIC_MAX(typ, name)                                                    \
    static inline typ name(typ v1, typ v2) { return v1 > v2 ? v1 : v2; }

#define EE_MAKE_FUNCTION_NUMERIC_MIN(typ, name)                                                    \
    static inline typ name(typ v1, typ v2) { return v1 < v2 ? v1 : v2; }

#define EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(typ, name)                                             \
    static inline typ name(typ *v1, typ v2)                                                        \
    {                                                                                              \
        *v1 = *v1 > v2 ? *v1 : v2;                                                                 \
        return *v1;                                                                                \
    }

#define EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(typ, name)                                             \
    static inline typ name(typ *v1, typ v2)                                                        \
    {                                                                                              \
        *v1 = *v1 < v2 ? *v1 : v2;                                                                 \
        return *v1;                                                                                \
    }

EE_MAKE_FUNCTION_NUMERIC_MAX(char, es_numeric_max_char)
EE_MAKE_FUNCTION_NUMERIC_MAX(short, es_numeric_max_short)
EE_MAKE_FUNCTION_NUMERIC_MAX(int, es_numeric_max_int)
EE_MAKE_FUNCTION_NUMERIC_MAX(long, es_numeric_max_long)
EE_MAKE_FUNCTION_NUMERIC_MAX(unsigned char, es_numeric_max_uchar)
EE_MAKE_FUNCTION_NUMERIC_MAX(unsigned short, es_numeric_max_ushort)
EE_MAKE_FUNCTION_NUMERIC_MAX(unsigned int, es_numeric_max_uint)
EE_MAKE_FUNCTION_NUMERIC_MAX(unsigned long, es_numeric_max_ulong)
EE_MAKE_FUNCTION_NUMERIC_MAX(float, es_numeric_max_float)
EE_MAKE_FUNCTION_NUMERIC_MAX(double, es_numeric_max_double)

EE_MAKE_FUNCTION_NUMERIC_MIN(char, es_numeric_min_char)
EE_MAKE_FUNCTION_NUMERIC_MIN(short, es_numeric_min_short)
EE_MAKE_FUNCTION_NUMERIC_MIN(int, es_numeric_min_int)
EE_MAKE_FUNCTION_NUMERIC_MIN(long, es_numeric_min_long)
EE_MAKE_FUNCTION_NUMERIC_MIN(unsigned char, es_numeric_min_uchar)
EE_MAKE_FUNCTION_NUMERIC_MIN(unsigned short, es_numeric_min_ushort)
EE_MAKE_FUNCTION_NUMERIC_MIN(unsigned int, es_numeric_min_uint)
EE_MAKE_FUNCTION_NUMERIC_MIN(unsigned long, es_numeric_min_ulong)
EE_MAKE_FUNCTION_NUMERIC_MIN(float, es_numeric_min_float)
EE_MAKE_FUNCTION_NUMERIC_MIN(double, es_numeric_min_double)

EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(char, es_numeric_max_update_char)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(short, es_numeric_max_update_short)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(int, es_numeric_max_update_int)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(long, es_numeric_max_update_long)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(unsigned char, es_numeric_max_update_uchar)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(unsigned short, es_numeric_max_update_ushort)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(unsigned int, es_numeric_max_update_uint)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(unsigned long, es_numeric_max_update_ulong)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(float, es_numeric_max_update_float)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(double, es_numeric_max_update_double)

EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(char, es_numeric_min_update_char)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(short, es_numeric_min_update_short)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(int, es_numeric_min_update_int)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(long, es_numeric_min_update_long)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(unsigned char, es_numeric_min_update_uchar)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(unsigned short, es_numeric_min_update_ushort)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(unsigned int, es_numeric_min_update_uint)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(unsigned long, es_numeric_min_update_ulong)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(float, es_numeric_min_update_float)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(double, es_numeric_min_update_double)

EE_MAKE_FUNCTION_NUMERIC_MAX(size_t, es_numeric_max_size)
EE_MAKE_FUNCTION_NUMERIC_MIN(size_t, es_numeric_min_size)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(size_t, es_numeric_max_update_size)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(size_t, es_numeric_min_update_size)

// Fixed width types

#if defined(INT8_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(int8_t, es_numeric_max_int8)
EE_MAKE_FUNCTION_NUMERIC_MIN(int8_t, es_numeric_min_int8)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(int8_t, es_numeric_max_update_int8)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(int8_t, es_numeric_min_update_int8)
#endif
#if defined(INT16_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(int16_t, es_numeric_max_int16)
EE_MAKE_FUNCTION_NUMERIC_MIN(int16_t, es_numeric_min_int16)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(int16_t, es_numeric_max_update_int16)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(int16_t, es_numeric_min_update_int16)
#endif
#if defined(INT32_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(int32_t, es_numeric_max_int32)
EE_MAKE_FUNCTION_NUMERIC_MIN(int32_t, es_numeric_min_int32)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(int32_t, es_numeric_max_update_int32)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(int32_t, es_numeric_min_update_int32)
#endif
#if defined(INT64_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(int64_t, es_numeric_max_int64)
EE_MAKE_FUNCTION_NUMERIC_MIN(int64_t, es_numeric_min_int64)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(int64_t, es_numeric_max_update_int64)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(int64_t, es_numeric_min_update_int64)
#endif

#if defined(UINT8_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(uint8_t, es_numeric_max_uint8)
EE_MAKE_FUNCTION_NUMERIC_MIN(uint8_t, es_numeric_min_uint8)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(uint8_t, es_numeric_max_update_uint8)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(uint8_t, es_numeric_min_update_uint8)
#endif
#if defined(UINT16_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(uint16_t, es_numeric_max_uint16)
EE_MAKE_FUNCTION_NUMERIC_MIN(uint16_t, es_numeric_min_uint16)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(uint16_t, es_numeric_max_update_uint16)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(uint16_t, es_numeric_min_update_uint16)
#endif
#if defined(UINT32_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(uint32_t, es_numeric_max_uint32)
EE_MAKE_FUNCTION_NUMERIC_MIN(uint32_t, es_numeric_min_uint32)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(uint32_t, es_numeric_max_update_uint32)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(uint32_t, es_numeric_min_update_uint32)
#endif
#if defined(UINT64_MAX)
EE_MAKE_FUNCTION_NUMERIC_MAX(uint64_t, es_numeric_max_uint64)
EE_MAKE_FUNCTION_NUMERIC_MIN(uint64_t, es_numeric_min_uint64)
EE_MAKE_FUNCTION_NUMERIC_MAX_UPDATE(uint64_t, es_numeric_max_update_uint64)
EE_MAKE_FUNCTION_NUMERIC_MIN_UPDATE(uint64_t, es_numeric_min_update_uint64)
#endif

#endif