#ifndef ES_LOG_H_INCLUDED
#define ES_LOG_H_INCLUDED 1

#define ES_LOG_LEVEL_OFF 0
#define ES_LOG_LEVEL_ERROR 1
#define ES_LOG_LEVEL_WARNING 2
#define ES_LOG_LEVEL_INFO 3
#define ES_LOG_LEVEL_DEBUG 4
#define ES_LOG_LEVEL_TRACE 5

#ifndef ES_LOG_LEVEL_ENABLED
#define ES_LOG_LEVEL_ENABLED ES_LOG_LEVEL_INFO
#endif

void
es_log__print(const char *file, int line, const char *component, int level, const char *fmt, ...);

#if ES_LOG_LEVEL_ENABLED >= ES_LOG_LEVEL_ERROR
#define es_log_errorf(component, fmt, ...)                                                         \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_ERROR, fmt, __VA_ARGS__)
#define es_log_error(component, ...)                                                               \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_ERROR, __VA_ARGS__)
#else
#define es_log_errorf(component, fmt, ...)
#define es_log_error(component, ...)
#endif

#if ES_LOG_LEVEL_ENABLED >= ES_LOG_LEVEL_WARNING
#define es_log_warningf(component, fmt, ...)                                                       \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_WARNING, fmt, __VA_ARGS__)
#define es_log_warning(component, ...)                                                             \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_WARNING, __VA_ARGS__)
#else
#define es_log_warningf(component, fmt, ...)
#define es_log_warning(component, ...)
#endif

#if ES_LOG_LEVEL_ENABLED >= ES_LOG_LEVEL_INFO
#define es_log_infof(component, fmt, ...)                                                          \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_INFO, fmt, __VA_ARGS__)
#define es_log_info(component, ...)                                                                \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_INFO, __VA_ARGS__)
#else
#define es_log_infof(component, fmt, ...)
#define es_log_info(component, ...)
#endif

#if ES_LOG_LEVEL_ENABLED >= ES_LOG_LEVEL_DEBUG
#define es_log_debugf(component, fmt, ...)                                                         \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_DEBUG, fmt, __VA_ARGS__)
#define es_log_debug(component, ...)                                                               \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_DEBUG, __VA_ARGS__)
#else
#define es_log_debugf(component, fmt, ...)
#define es_log_debug(component, ...)
#endif

#if ES_LOG_LEVEL_ENABLED >= ES_LOG_LEVEL_TRACE
#define es_log_tracef(component, fmt, ...)                                                         \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_TRACE, fmt, __VA_ARGS__)
#define es_log_trace(component, ...)                                                               \
	es_log__print(__FILE__, __LINE__, component, ES_LOG_LEVEL_TRACE, __VA_ARGS__)
#else
#define es_log_tracef(component, fmt, ...)
#define es_log_trace(component, ...)
#endif

#define es_log_name(var) (void)var;

#endif