#ifndef EE_ES_COMPARE_H_INCLUDED
#define EE_ES_COMPARE_H_INCLUDED 1

#include <stdint.h>

int es_compare_char(const void *p1, const void *p2);
int es_compare_short(const void *p1, const void *p2);
int es_compare_int(const void *p1, const void *p2);
int es_compare_long(const void *p1, const void *p2);

int es_compare_uchar(const void *p1, const void *p2);
int es_compare_ushort(const void *p1, const void *p2);
int es_compare_uint(const void *p1, const void *p2);
int es_compare_ulong(const void *p1, const void *p2);

int es_compare_size(const void *p1, const void *p2);

#if defined(UINT8_MAX)
int es_compare_uint8(const void *p1, const void *p2);
#endif
#if defined(UINT16_MAX)
int es_compare_uint16(const void *p1, const void *p2);
#endif
#if defined(UINT32_MAX)
int es_compare_uint32(const void *p1, const void *p2);
#endif
#if defined(UINT64_MAX)
int es_compare_uint64(const void *p1, const void *p2);
#endif
#if defined(INT8_MAX)
int es_compare_int8(const void *p1, const void *p2);
#endif
#if defined(INT16_MAX)
int es_compare_int16(const void *p1, const void *p2);
#endif
#if defined(INT32_MAX)
int es_compare_int32(const void *p1, const void *p2);
#endif
#if defined(INT64_MAX)
int es_compare_int64(const void *p1, const void *p2);
#endif

#endif