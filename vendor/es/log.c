#include <es/log.h>

#include <stdio.h>
#include <stdarg.h>

static const char *level_strings[] = {
    [ES_LOG_LEVEL_OFF] = "off",
    [ES_LOG_LEVEL_ERROR] = "error",
    [ES_LOG_LEVEL_WARNING] = "warning",
    [ES_LOG_LEVEL_INFO] = "info",
    [ES_LOG_LEVEL_DEBUG] = "debug",
    [ES_LOG_LEVEL_TRACE] = "trace",
};

static const char *level_color[] = {
    [ES_LOG_LEVEL_OFF] = "off",
    [ES_LOG_LEVEL_ERROR] = "\033[31m",
    [ES_LOG_LEVEL_WARNING] = "\033[33m",
    [ES_LOG_LEVEL_INFO] = "\033[37m",
    [ES_LOG_LEVEL_DEBUG] = "\033[36m",
    [ES_LOG_LEVEL_TRACE] = "\033[34m",
};

void es_log__print(const char *file, int line, const char *component, int level, const char *fmt, ...)
{
    fprintf(stderr, "[%s%7s\033[0m] [%s] ", level_color[level], level_strings[level], component);
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
    fprintf(stderr, "\t(%s:%d)\n", file, line);
}