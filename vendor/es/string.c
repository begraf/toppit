#include <es/string.h>

#include <es/es.h>

#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

bool es_cstr_is_empty(const char *string)
{
    return !string || string[0] == '\0';
}

bool es_cstr_is_ascii(const char *string)
{
    for (; *string; string++)
    {
        if (*(unsigned char *)string > 127)
        {
            return false;
        }
    }
    return true;
}

char *es_cstr_append(char *dst, const char *src)
{
    for (; *src; *dst++ = *src++)
        ;
    *dst = '\0';
    return dst;
}

char *es_cstr_join_by(const char **strings, size_t n, const char *separator)
{
    size_t length = 0;
    for (size_t i = 0; i < n; i++)
    {
        length += strlen(strings[i]);
    }

    const size_t separator_length = strlen(separator);
    length += n * separator_length;

    char *buffer = calloc(length + 1, 1);
    if (!buffer)
        return NULL;

    if (n > 0)
    {
        char *run = es_cstr_append(buffer, strings[0]);
        for (size_t i = 1; i < n; i++)
        {
            run = es_cstr_append(run, separator);
            run = es_cstr_append(run, strings[i]);
        }
    }

    return buffer;
}

char *es_cstr_join(const char **strings, size_t n)
{
    return es_cstr_join_by(strings, n, "");
}

size_t es_cstr_size(const char *string)
{
    // TODO: how to handle null?
    return strlen(string);
}

char *es_cstr_trim_whitespace(char *string)
{
    es_memory_return_null(string);
    char *s = es_cstr_skip_whitespace(string);
    size_t len = es_cstr_size(s);
    memmove(string, s, len + 1);
    while (len > 0 && isspace(string[len - 1]))
    {
        string[--len] = '\0';
    }
    return string;
}

char *es_cstr_trim_whitespace_left(char *string)
{
    es_memory_return_null(string);
    char *s = es_cstr_skip_whitespace(string);
    const size_t len = es_cstr_size(s);
    memmove(string, s, len + 1);
    return string;
}

char *es_cstr_trim_whitespace_right(char *string)
{
    es_memory_return_null(string);
    for (size_t n = strlen(string); n > 0 && isspace(string[n - 1]);)
    {
        string[--n] = '\0';
    }
    return string;
}

char *es_cstr_skip_whitespace(char *string)
{
    es_memory_return_null(string);
    for (; *string != '\0' && isspace(*string); string++)
        ;
    return string;
}

bool es_cstr_equal(const char *string1, const char *string2)
{
    // TODO: howto handle null
    return !es_cstr_compare(string1, string2);
}

int es_cstr_compare(const char *string1, const char *string2)
{
    // TODO: howto handle null
    return strcmp(string1, string2);
}

char *es_cstr_dup(const char *string)
{
    es_memory_return_null(string);
    const size_t len = strlen(string);
    char *buffer = malloc(len + 1);
    if (!buffer)
    {
        return 0;
    }
    strncpy(buffer, string, len + 1);
    return buffer;
}

///////////////////////////////////////////////////////////////////////////////
///                         Searching and counting                          ///
///////////////////////////////////////////////////////////////////////////////

char *es_cstr_find_char(const char *str, char key)
{
    if (!str)
        return 0;
    return strchr(str, key);
}

char *es_cstr_find_char_by(const char *str, int (*pred)(int))
{
    if (!str)
    {
        return 0;
    }
    for (; *str; str++)
    {
        if (pred(*str))
        {
            return (char *)str;
        }
    }
    return 0;
}

size_t es_cstr_count_char(const char *str, char key)
{
    size_t cnt = 0;
    while ((str = es_cstr_find_char(str, key)) != 0)
    {
        cnt++;
        str++;
    }
    return cnt;
}

size_t es_cstr_count_char_by(const char *str, int (*pred)(int))
{
    if (!str)
    {
        return 0;
    }
    size_t cnt = 0;
    for (; *str; str++)
    {
        if (pred(*str))
        {
            cnt++;
        }
    }
    return cnt;
}

///////////////////////////////////////////////////////////////////////////////
///                         Conversions and filters                         ///
///////////////////////////////////////////////////////////////////////////////

void es_cstr_to_lower(char *str)
{
    if (!str)
        return;
    for (; *str; str++)
    {
        *str = tolower(*str);
    }
}

void es_cstr_to_upper(char *str)
{
    if (!str)
        return;
    for (; *str; str++)
    {
        *str = toupper(*str);
    }
}

///////////////////////////////////////////////////////////////////////////////
///                              Non null API                               ///
///////////////////////////////////////////////////////////////////////////////

char *es_cstr_join_nn(const char **strings, size_t n)
{
    char *result = es_cstr_join(strings, n);
    if (!result)
    {
        abort();
    }
    return result;
}

char *es_cstr_join_by_nn(const char **strings, size_t n, const char *separator)
{
    char *result = es_cstr_join_by(strings, n, separator);
    if (!result)
    {
        abort();
    }
    return result;
}

char *es_cstr_dup_nn(const char *string)
{
    char *buffer = es_cstr_dup(string);
    if (!buffer)
    {
        abort();
    }
    return buffer;
}
