#include <es/compare.h>

#include <stddef.h>

int es_compare_char(const void *p1, const void *p2)
{
    return *(const char *)p1 - *(const char *)p2;
}

int es_compare_short(const void *p1, const void *p2)
{
    return *(const short *)p1 - *(const short *)p2;
}

int es_compare_int(const void *p1, const void *p2)
{
    return *(const int *)p1 - *(const int *)p2;
}

int es_compare_long(const void *p1, const void *p2)
{
    return *(const long *)p1 - *(const long *)p2;
}

int es_compare_uchar(const void *p1, const void *p2)
{
    return *(const unsigned char *)p1 - *(const unsigned char *)p2;
}

int es_compare_ushort(const void *p1, const void *p2)
{
    return *(const unsigned short *)p1 - *(const unsigned short *)p2;
}

int es_compare_uint(const void *p1, const void *p2)
{
    return *(const unsigned int *)p1 - *(const unsigned int *)p2;
}

int es_compare_ulong(const void *p1, const void *p2)
{
    return *(const unsigned long *)p1 - *(const unsigned long *)p2;
}

int es_compare_size(const void *p1, const void *p2)
{
    return *(const size_t *)p1 - *(const size_t *)p2;
}

#if defined(UINT8_MAX)
int es_compare_uint8(const void *p1, const void *p2)
{
    return *(const uint8_t *)p1 - *(const uint8_t *)p2;
}
#endif
#if defined(UINT16_MAX)
int es_compare_uint16(const void *p1, const void *p2)
{
    return *(const uint16_t *)p1 - *(const uint16_t *)p2;
}
#endif
#if defined(UINT32_MAX)
int es_compare_uint32(const void *p1, const void *p2)
{
    return *(const uint32_t *)p1 - *(const uint32_t *)p2;
}
#endif
#if defined(UINT64_MAX)
int es_compare_uint64(const void *p1, const void *p2)
{
    return *(const uint64_t *)p1 - *(const uint64_t *)p2;
}
#endif
#if defined(INT8_MAX)
int es_compare_int8(const void *p1, const void *p2)
{
    return *(const int8_t *)p1 - *(const int8_t *)p2;
}
#endif
#if defined(INT16_MAX)
int es_compare_int16(const void *p1, const void *p2)
{
    return *(const int16_t *)p1 - *(const int16_t *)p2;
}
#endif
#if defined(INT32_MAX)
int es_compare_int32(const void *p1, const void *p2)
{
    return *(const int32_t *)p1 - *(const int32_t *)p2;
}
#endif
#if defined(INT64_MAX)
int es_compare_int64(const void *p1, const void *p2)
{
    return *(const int64_t *)p1 - *(const int64_t *)p2;
}
#endif
